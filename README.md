# StateChart

A Cinco example for modeling and simulating state charts.

![Stop and Go](readme-files/stop-and-go.png)



## Table of contents

1. [Running StateChart](#1-running-statechart)
	1. [Cinco](#11-cinco)
	2. [Import StateChart project](#12-import-statechart-project)
	3. [Generate Cinco product](#13-generate-cinco-product)
	4. [Generate Guard Expression Language](#14-generate-guard-expression-language)
	5. [Generate Action Expression Language](#15-generate-action-expression-language)
	6. [Run StateChart](#16-run-statechart)
	7. [Troubleshooting](#17-troubleshooting)
2. [Using StateChart](#2-using-statechart)
	1. [Create a StateChart project](#21-create-a-statechart-project)
	2. [Create StateChart file](#22-create-statechart-file)
	3. [Edit a StateChart file](#23-edit-a-statechart-file)
	4. [Simulating a StateChart](#24-simulating-a-statechart)



## 1. Running StateChart

This section describes the necessary steps to get StateChart up and running.



### 1.1. Cinco

1. Install and start [Cinco 2.0](https://cinco.scce.info/download/)
2. Click "File → Switch Workspace → Other"
3. Choose a new location
4. Click "Launch"
5. Click "Window → Preferences"
6. Under "Java → Compiler" select "Comiler complience level: 11"
7. Restart Cinco

![Comiler complience level](readme-files/compiler-complience-level.png)



### 1.2. Import StateChart project

1. Click "File → Import"
2. Select "Git → Projects from Git"
3. Click "Next"
4. Select "Clone URI"
5. Click "Next"
6. Enter for "URI": `https://gitlab.com/scce/cinco-examples/statechart.git`
7. Click "Next" and follow the wizzard

![Import from Git](readme-files/import-from-git.png)



### 1.3. Generate Cinco product

1. Right click on `info.scce.cinco.product.statechart/model/StateChartTool.cpd`
2. Click "Generate Cinco Product"
3. Wait until finished

![Generate Cinco Product](readme-files/generate-cinco-product.png)



### 1.4. Generate Guard Expression Language

1. Right click on `info.scce.cinco.product.statechart.dsl.guard/src/info/scce/cinco/product/statechart/dsl/guard/GenerateGuardExpression.mwe2`
2. Click "Run As → MWE2 Workflow"
3. Wait until finished
4. If exceptions are thrown, refer to [1.7. Troubleshooting](#17-troubleshooting)

![Generate Guard Expression Language](readme-files/generate-guard-expression-language.png)



### 1.5. Generate Action Expression Language

1. Right click on `info.scce.cinco.product.statechart.dsl.action/src/info/scce/cinco/product/statechart/dsl/action/GenerateActionExpression.mwe2`
2. Click "Run As → MWE2 Workflow"
3. Wait until finished
4. If exceptions are thrown, refer to [1.7. Troubleshooting](#17-troubleshooting)

![Generate Action Expression Language](readme-files/generate-action-expression-language.png)



### 1.6. Run StateChart

1. Right click on `info.scce.cinco.product.statechart`
2. Click "Run As → Eclipse Application"

![Run StateChart](readme-files/run-statechart.png)



### 1.7. Troubleshooting

If the generation of the expression languages throws exceptions, try creating a new `StateChart.genmodel`. This may be necessary after changing the `info.scce.cinco.product.statechart/model/StateChart.mgl` and regenerating the Cinco product. This is because the expression languages use the StateChart models.

1. Delete old files
	1. Delete `info.scce.cinco.product.statechart/src/model/StateChart.ecore`
	2. Delete `info.scce.cinco.product.statechart/src/model/StateChart.genmodel`
2. Recreate the files
	1. Click "File → New → Other"
	2. Select "Eclipse Modeling Framework → EMF Generator Model"
	3. Click "Next"
	4. Enter for "Parent folder": `info.scce.cinco.product.statechart/src/model`
	5. Enter for "File name": `StateChart.genmodel`
	6. Click "Next"
	7. Select "Ecore model"
	8. Click "Next"
	9. Enter for "Model URIs": `platform:/resource/info.scce.cinco.product.statechart/src-gen/model/StateChart.ecore`
	10. Click "Load"
	11. Click "Next"
	12. Under "Root packages" select "statechart"
	13. Under "Referenced generator models" select "GraphModel"
	14. Click "Finish"
3. Edit the GenModel file
	1. Open `info.scce.cinco.product.statechart/src/model/StateChart.genmodel`
	2. Expand "StateChart"
	3. Select "Statechart"
	4. Open the "Properties" view
		1. Click "Window → Show View → Other"
		2. Select "General → Properties"
		3. Click "Open"
	5. Under "All" enter for "Base Package": `info.scce.cinco.product.statechart`
	6. Save the GenModel file
4. Retry generating the expression languages
	1. Generate the Guard Expression Language
	2. Generate the Action Expression Language



## 2. Using StateChart

This section describes how to use StateChart.



### 2.1. Create a StateChart project

1. Click "File → New → Other"
2. Select "Cinco Product → New StateChartTool Project"
3. Click "Next" and follow the wizzard



### 2.2. Create a StateChart file

1. Click "File → New → Other"
2. Select "Cinco Product → New StateChart"
3. Click "Next" and follow the wizzard



### 2.3. Edit a StateChart file

1. Open the StateChart file
2. Drag elements from the palette on the right into the workspace
	- Make sure that the StateChart has (exactly) one "Start" node
	- In order to declare variables and triggers, drag a "DeclarationContainer" into the workspace and any "Declaration" (Integer, Boolean, Trigger) into the "DeclarationContainer"
	- In order to edit properties of nodes, containers and edges select the element and open the "Cinco Properties" view
		1. Click "Window → Show View → Other"
		2. Select "Other → Cinco Properties"
		3. Click "Open"
3. Save the StateChart file

![StateChart editing](readme-files/statechart-editing.png)



### 2.4. Simulate a StateChart

1. Open the StateChart file
2. Click "StateChart → Simulate state chart"
3. Click on the buttons in the "Triggers" view to trigger a trigger
4. Observe and edit variables in the "Variables" view

![StateChart simulation](readme-files/statechart-simulation.png)
