package info.scce.cinco.product.statechart.simulator

import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AddSubExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AndExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AtomicArithmeticExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AtomicBooleanExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.EqualsTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.GreaterOrEqualsTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.GreaterThanTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.GuardExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.LessOrEqualsTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.LessThanTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MinusExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MulDivExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NandExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NorExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NotEqualsTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NotExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.OrExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.PowExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.RelationalExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.XnorExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.XorExpression
import info.scce.cinco.product.statechart.statechart.BooleanDeclaration

import static extension info.scce.cinco.product.statechart.dsl.guard.extensions.GuardExpressionExtension.*
import static extension info.scce.cinco.product.statechart.simulator.IntegerEvaluator.*

class BooleanEvaluator {
	
	/**
	 * Evaluates a guard. The guard is a string formatted in the {@link
	 * info.scce.cinco.product.statechart.dsl.guard.guardExpression.GuardExpression
	 * GuardExpression} language (guard). Returns the evaluated value of the
	 * expression or {@code true} if the string is {@code null}, empty or the
	 * default guard.
	 */
	static def dispatch boolean evaluateBoolean(String code) {
		if (code.nullOrEmpty) return true
		code.parseGuard.evaluateBoolean
	}
	
	static def dispatch boolean evaluateBoolean(Void it) {
		return true
	}
	
	static def dispatch boolean evaluateBoolean(GuardExpression it) {
		if (defaultGuard || elseGuard) return true
		if (isBooleanExpression) return term.evaluateBoolean
		return false
	}
	
	static def dispatch boolean evaluateBoolean(OrExpression it) {
		return right.fold(left.evaluateBoolean) [l, r | l || r.evaluateBoolean]
	}
	
	static def dispatch boolean evaluateBoolean(NorExpression it) {
		return right.fold(left.evaluateBoolean) [l, r | !(l || r.evaluateBoolean)]
	}
	
	static def dispatch boolean evaluateBoolean(AndExpression it) {
		return right.fold(left.evaluateBoolean) [l, r | l && r.evaluateBoolean]
	}
	
	static def dispatch boolean evaluateBoolean(NandExpression it) {
		return right.fold(left.evaluateBoolean) [l, r | !(l && r.evaluateBoolean)]
	}
	
	static def dispatch boolean evaluateBoolean(XorExpression it) {
		return right.fold(left.evaluateBoolean) [l, r | l != r.evaluateBoolean]
	}
	
	static def dispatch boolean evaluateBoolean(XnorExpression it) {
		return right.fold(left.evaluateBoolean) [l, r | l == r.evaluateBoolean]
	}
	
	static def dispatch boolean evaluateBoolean(NotExpression it) {
		return if (not) !term.evaluateBoolean else term.evaluateBoolean
	}
	
	static def dispatch boolean evaluateBoolean(AtomicBooleanExpression it) {
		if (literal !== null) {
			return if (literal == 'true') true else false
		}
		else {
			return term.evaluateBoolean
		}
	}
	
	static def dispatch boolean evaluateBoolean(RelationalExpression it) {
		return switch right {
			case null:           left.evaluateBoolean
			EqualsTerm:          left.evaluateInteger == right.term.evaluateInteger
			NotEqualsTerm:       left.evaluateInteger != right.term.evaluateInteger
			LessThanTerm:        left.evaluateInteger <  right.term.evaluateInteger
			LessOrEqualsTerm:    left.evaluateInteger <= right.term.evaluateInteger
			GreaterThanTerm:     left.evaluateInteger >  right.term.evaluateInteger
			GreaterOrEqualsTerm: left.evaluateInteger >= right.term.evaluateInteger
			default:             throw new IllegalArgumentException('''The relational expression cannot be evaluated to a boolean, because the type of the right side "«right.class.name»" is unknown.''')
		}
	}
	
	static def dispatch boolean evaluateBoolean(AddSubExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The add/sub expression cannot be evaluated to a boolean, because the right side is not null.')
		return left.evaluateBoolean
	}
	
	static def dispatch boolean evaluateBoolean(MulDivExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The mul/div expression cannot be evaluated to a boolean, because the right side is not null.')
		return left.evaluateBoolean
	}
	
	static def dispatch boolean evaluateBoolean(PowExpression it) {
		if (right !== null) throw new IllegalArgumentException('The pow expression cannot be evaluated to a boolean, because the right side is not null.')
		return left.evaluateBoolean
	}
	
	static def dispatch boolean evaluateBoolean(MinusExpression it) {
		if (minus) throw new IllegalArgumentException('The minus expression cannot be evaluated to a boolean, because it has a minus.')
		return term.evaluateBoolean
	}
	
	static def dispatch boolean evaluateBoolean(AtomicArithmeticExpression it) {
		if (term !== null) {
			return term.evaluateBoolean
		}
		else if (reference === null) {
			throw new IllegalArgumentException('The atomic arithmetic expression cannot be evaluated to a boolean, because it is a literal integer.')
		}
		else if (reference instanceof BooleanDeclaration) {
			val extension simulator = Simulator.instance
			return (reference as BooleanDeclaration).value
		}
		else {
			throw new IllegalArgumentException('The atomic arithmetic expression cannot be evaluated to a boolean, because it is reference to an integer.')
		}
	}
	
}