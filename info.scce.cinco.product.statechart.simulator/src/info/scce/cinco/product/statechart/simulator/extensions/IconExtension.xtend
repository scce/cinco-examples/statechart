package info.scce.cinco.product.statechart.simulator.extensions

import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Platform
import org.eclipse.jface.resource.ImageDescriptor

class IconExtension {
	
	/**
	 * Returns the image descriptor of the icon with the provided name.
	 */
	static def getImageDescriptor(String name) {
		return ImageDescriptor.createFromFile(IconExtension, '''/icons/«name».png''')
	}
	
	/**
	 * Returns the path to the icon with the provided name.
	 */
	static def getPath(String name) {
		val bundle = Platform.getBundle('info.scce.cinco.product.statechart.simulator')
		val url = FileLocator.resolve(bundle.getEntry('''/icons/«name».png'''))
		return url.path
	}
	
}
