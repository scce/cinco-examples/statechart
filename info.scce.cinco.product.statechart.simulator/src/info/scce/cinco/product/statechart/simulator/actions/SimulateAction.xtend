package info.scce.cinco.product.statechart.simulator.actions

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import info.scce.cinco.product.statechart.simulator.Simulator
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException

import static extension info.scce.cinco.product.statechart.extensions.StateChartExtension.*

class SimulateAction extends AbstractHandler {
	
	extension WorkbenchExtension = new WorkbenchExtension

	override Object execute(ExecutionEvent event) throws ExecutionException {
		
		val simulator = Simulator.instance
		
		if (activeStateChart === null) {
			showErrorDialog('No active state chart', 'There is no active state chart in the editor. Please open a state chart and try again.')
			return null
		}
		
		if (simulator.initialized && !simulator.simulationEnded) {
			val restart = showQuestionDialog('Running simulation', 'Do you wish to abort the currently running simulation and start a new one?')
			if (!restart) return null
		}
		
		simulator.initialize(activeStateChart)
		return null
		
	}

}
