package info.scce.cinco.product.statechart.simulator

import info.scce.cinco.product.statechart.statechart.BooleanDeclaration
import info.scce.cinco.product.statechart.statechart.IntegerDeclaration

import static extension info.scce.cinco.product.statechart.dsl.action.extensions.ActionExpressionExtension.*
import static extension info.scce.cinco.product.statechart.dsl.guard.extensions.GuardExpressionExtension.*
import static extension info.scce.cinco.product.statechart.simulator.BooleanEvaluator.*
import static extension info.scce.cinco.product.statechart.simulator.IntegerEvaluator.*

class ActionEvaluator {
	
	/**
	 * Evaluates an action. The action is a string formatted in the {@link
	 * info.scce.cinco.product.statechart.dsl.action.actionExpression.ActionExpression
	 * ActionExpression} language (action).
	 */
	static def void evaluateAction(String code) {
		if (code.nullOrEmpty) { return }
		val extension simulator = Simulator.instance
		val instructions = code.parseAction.instructions
		if (instructions.nullOrEmpty) { return }
		for (i: instructions) {
			if (i.reference instanceof BooleanDeclaration && i.term.isBooleanExpression) {
				(i.reference as BooleanDeclaration).value = i.term.evaluateBoolean
			}
			else if (i.reference instanceof IntegerDeclaration && i.term.isArithmeticExpression) {
				(i.reference as IntegerDeclaration).value = i.term.evaluateInteger
			}
			else {
				throw new IllegalArgumentException('''Action "«code»" cannot be performed because of a type mismatch: The variable "«i.reference.label»" («i.reference.class.simpleName») is not compatible with this expression.''')
			}
		}
	}
	
}
