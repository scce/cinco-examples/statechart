package info.scce.cinco.product.statechart.simulator

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.cinco.product.statechart.simulator.views.TriggerView
import info.scce.cinco.product.statechart.simulator.views.VariableView
import info.scce.cinco.product.statechart.statechart.AtomicHistory
import info.scce.cinco.product.statechart.statechart.BooleanDeclaration
import info.scce.cinco.product.statechart.statechart.BranchAndMerge
import info.scce.cinco.product.statechart.statechart.End
import info.scce.cinco.product.statechart.statechart.GuardedTransition
import info.scce.cinco.product.statechart.statechart.History
import info.scce.cinco.product.statechart.statechart.IntegerDeclaration
import info.scce.cinco.product.statechart.statechart.NormalHistory
import info.scce.cinco.product.statechart.statechart.ParallelLane
import info.scce.cinco.product.statechart.statechart.SimpleTransition
import info.scce.cinco.product.statechart.statechart.Start
import info.scce.cinco.product.statechart.statechart.State
import info.scce.cinco.product.statechart.statechart.StateChart
import info.scce.cinco.product.statechart.statechart.StateTransition
import info.scce.cinco.product.statechart.statechart.Transition
import info.scce.cinco.product.statechart.statechart.VariableDeclaration
import java.util.ArrayDeque
import java.util.HashMap
import java.util.HashSet
import org.eclipse.xtend.lib.annotations.Accessors

import static extension info.scce.cinco.product.statechart.dsl.guard.extensions.GuardExpressionExtension.*
import static extension info.scce.cinco.product.statechart.extensions.ContainerExtension.*
import static extension info.scce.cinco.product.statechart.extensions.HistoryExtension.*
import static extension info.scce.cinco.product.statechart.extensions.ModelElementExtension.*
import static extension info.scce.cinco.product.statechart.extensions.NodeExtension.*
import static extension info.scce.cinco.product.statechart.extensions.ParallelLaneExtension.*
import static extension info.scce.cinco.product.statechart.extensions.StartExtension.*
import static extension info.scce.cinco.product.statechart.extensions.StateChartExtension.*
import static extension info.scce.cinco.product.statechart.extensions.StateExtension.*
import static extension info.scce.cinco.product.statechart.extensions.TransitionExtension.*
import static extension info.scce.cinco.product.statechart.simulator.ActionEvaluator.*
import static extension info.scce.cinco.product.statechart.simulator.BooleanEvaluator.*

class Simulator {
	
	
	
	/* XXX: Extensions ************************************************************/
	
	

	static extension GraphModelExtension = new GraphModelExtension
	static extension WorkbenchExtension  = new WorkbenchExtension
	
	
	
	/* XXX: Attributes ************************************************************/
	
	
	
	/**  The singleton instance of this class. */
	static Simulator simulatorInstance
		
	/** The state chart that is simulated. */
	@Accessors(PUBLIC_GETTER)
	public StateChart stateChart
	
	/** A set of all currently active model elements in the state chart. */
	HashSet<ModelElement> activeElements
		
	/** Maps the name of a variable to its current value. */
	HashMap<BooleanDeclaration, Boolean> booleanVariables
	
	/** Maps the name of a variable to its current value. */
	HashMap<IntegerDeclaration, Integer> integerVariables
	
	/** Maps history nodes to a maps of elements and their historic activation state. */
	HashMap<History, HashMap<ModelElement, Boolean>> histories
	
	/** The instance of the trigger view. */
	@Accessors(PUBLIC_GETTER)
	TriggerView triggerView
	
	/** The instance of the variable view. */
	@Accessors(PUBLIC_GETTER)
	VariableView variableView
	
	/** Wheather or not the trigger and variable views are enabled. */
	boolean enabled
	
	/** Weather or not debug information should be printed to {@link System#out System.out}. */
	static boolean debug = false
	
	/** A stack, representing an action and its consequent actions. (Debugging purposes only!) */
	ArrayDeque<Object> debugStack
	
	
	
	/* XXX: Instance & Initialization *********************************************/
	
	
	
	/**
	 * The constructor is private to ensure there is only one instance of the
	 * simulator. To get the instance, use {@link Simulator#getInstance()
	 * getInstance()}.
	 */
	private new() {
		// Intentionally left blank
	}

	/**
	 * Returns the singleton instance of the Simulator.
	 */
	static synchronized def Simulator getInstance() {
		if (Simulator.simulatorInstance === null) {
			Simulator.simulatorInstance = new Simulator
		}
		return Simulator.simulatorInstance
	}
	
	/**
	 * Initializes the simulator for the provided StateChart
	 */
	def void initialize(StateChart it) {
		
		if (it === null) {
			throw new NullPointerException('Cannot initialize a state chart that is null.')
		}
		
		// Deinitialize
		deinitialize
		
		// Initialize fields
		stateChart = it
		activeElements = newHashSet
		histories = newHashMap
		booleanVariables = newHashMap
		integerVariables = newHashMap
		stateChart.booleanDeclarations.forEach[booleanVariables.put(it, parsedStartValue)]
		stateChart.integerDeclarations.forEach[integerVariables.put(it, parsedStartValue)]
		
		// Show and enable accessory views
		triggerView = TriggerView.show
		variableView = VariableView.show
		stateChart.show
		setEnabled(true)
		
		// Debug output
		if (debug) {
			debugStack = new ArrayDeque<Object>
			println
			println(debugIndentation + '********************************************************************************')
			println(debugIndentation + '''Starting simulation of state chart "«stateChart.name»"''')
			println(debugIndentation + '********************************************************************************')
			println
		}
		
		// Activate the first start node in the model
		stateChart.start.activate
		
	}
	
	
	
	/* XXX: Getter & Setter *******************************************************/
	
	
	
	/**
	 * Returns the value of the desired state chart variable.
	 */
	def boolean getValue(BooleanDeclaration it) {
		return booleanVariables.get(variable)
	}
	
	/**
	 * Returns the value of the desired state chart variable.
	 */
	def int getValue(IntegerDeclaration it) {
		return integerVariables.get(variable)
	}
	
	/**
	 * Sets the value of a state chart variable. Any state transitions with a
	 * default trigger and a guard, that evaluates to {@code true} after the
	 * variable change will be activated.
	 */
	def Object setValue(BooleanDeclaration it, boolean value) {
		if (simulationEnded) return null
		booleanVariables.put(variable, value)
		variableView.refresh
		updateGuards(variable)
		return value
	}
	
	/**
	 * Sets the value of a state chart variable. Any state transitions with a
	 * default trigger and a guard, that evaluates to {@code true} after the
	 * variable change will be activated.
	 */
	def Object setValue(IntegerDeclaration it, int value) {
		if (simulationEnded) return null
		integerVariables.put(variable, value)
		variableView.refresh
		updateGuards(variable)
		return value
	}
	
	/**
	 * Returns a variable declaration that is in the {@link #variables variables}
	 * map. If {@code it} is a key of the {@code variable} map, {@code it} itself
	 * will be returned. If {@code it} is not a key of the {@code variable} map,
	 * a valid key with the same label or {@code null} will be returned. 
	 */
	def BooleanDeclaration getVariable(BooleanDeclaration it) {
		if (simulationEnded) return null
		var BooleanDeclaration result
		if (booleanVariables.containsKey(it)) {
			result = it
		}
		else {
			result = booleanVariables.keySet.findFirst[v | label == v.label]
		}
		if (result === null) {
			throw new IllegalArgumentException('''The variable declaration for "«label»" could not be found.''')
		}
		else {
			return result
		}
	}
	
	/**
	 * Returns a variable declaration that is in the {@link #variables variables}
	 * map. If {@code it} is a key of the {@code variable} map, {@code it} itself
	 * will be returned. If {@code it} is not a key of the {@code variable} map,
	 * a valid key with the same label or {@code null} will be returned. 
	 */
	def IntegerDeclaration getVariable(IntegerDeclaration it) {
		if (simulationEnded) return null
		var IntegerDeclaration result
		if (integerVariables.containsKey(it)) {
			result = it
		}
		else {
			result = integerVariables.keySet.findFirst[v | label == v.label]
		}
		if (result === null) {
			throw new IllegalArgumentException('''The variable declaration for "«label»" could not be found.''')
		}
		else {
			return result
		}
	}
	
	/**
	 * Returns a boolean whether the model element is marked as active or not.
	 */
	def boolean isActive(ModelElement it) {
		return activeElements.contains(it)
	}
	
	/**
	 * <p>Marks a model element as active by adding it to {@link #activeElements
	 * activeElements} and highlighting it. Or marks a node as inactive by
	 * removing it from {@code activeElements} and de-highlighting it.</p>
	 * <p>This is different from the {@link #activate(ModelElement) activate(ModelElement)}
	 * method. {@code setActive(ModelElement, boolean)} simply marks the element
	 * as active and acts as a convenience method. {@code activate(ModelElement)}
	 * does further work depending on the type of node, such as automatically
	 * activating successors.</p>
	 */
	def boolean setActive(ModelElement it, boolean value) {		
		if (simulationEnded) { return value }
		highlight = value
		if (value) {
			activeElements.add(it)
			if (it instanceof Node) {
				if (parentContainer !== null) {
					parentContainer.active = true
				}
			}
		}
		else {
			activeElements.remove(it)
		}
		return value
	}
	
	
	
	/* XXX: Trigger ***************************************************************/
	
	
	
	/**
	 * Checks, if a given transition can be triggered with the provided trigger.
	 * This also checks, if the transition's guard is satisfied (if available)
	 * and if the source element can be deactivated and if the target element
	 * can be activated.
	 */
	def boolean canTrigger(Transition it, String triggerName) {
		if (!initialized || simulationEnded || it === null || triggerName.nullOrEmpty ||
			!sourceElement.canDeactivate || !targetElement.canActivate) {
			return false
		}
		switch (it) {
			SimpleTransition: {
				triggerName == defaultTrigger
			}
			GuardedTransition: {
				if (triggerName != defaultTrigger) {
					return false
				}
				val guardExpr = guard.parseGuard
				if (guardExpr.elseGuard) {
					return siblings.forall[s | !s.guard.evaluateBoolean]
				}
				else {
					return guardExpr.evaluateBoolean
				}
			}
			StateTransition: {
				if (triggerName != trigger) {
					return false
				}
				val guardExpr = guard.parseGuard
				if (guardExpr.elseGuard) {
					return siblings.filter[s | triggerName == s.trigger].forall[s | !s.guard.evaluateBoolean]
				}
				else {
					return guardExpr.evaluateBoolean
				}
			}
			default: {
				throw new IllegalArgumentException('''Unknown transition type "«class.name»".''')
			}
		}
	}
	
	/**
	 * Triggers the transition with the provided trigger name for one transition.
	 */
	def void doTrigger(Transition it, String triggerName) {
		if (canTrigger(triggerName)) {
			if (!siblings.exists[s | s.priority > priority && s.canTrigger(triggerName)]) {
				debugPush('''Triggering "«triggerName»" on''')
				activate
				doAction
				targetElement.activate
				debugPop
			}
		}
	}
	
	/**
	 * Triggers the transition with the default trigger for one transition.
	 */
	def void doTrigger(Transition it) {
		doTrigger(it, defaultTrigger)
	}
	
	/**
	 * Triggers the trigger with the provided name on the whole state chart.
	 */
	def void doTrigger(String triggerName) {
		
		if (initialized && !simulationEnded && !triggerName.nullOrEmpty) {
			
			debugPush(stateChart, '''User triggering "«triggerName»" on''')
			
			// Trigger all outgoing transitions from active states
			val activeStates = stateChart.find(State).filter[active].toList
			val transitions = activeStates.map[outgoingStateTransitions].flatten.toList
			for (transition: transitions) {
				transition.doTrigger(triggerName)
				if (simulationEnded) {
					setEnabled(false)
					val restart = showQuestionDialog('Simulation ended', 'The simulation did come to an end. Do you wish to restart the simulation?')
					if (restart) {
						initialize(stateChart)
					}
					return
				}
			}
			
			debugPop(stateChart)
		
		}
		
	}
	
	
	
	/* XXX: Activate **************************************************************/
	
	
	
	/**
	 * Checks, if a model element can be activated or not.
	 */
	def boolean canActivate(ModelElement it) {
		return initialized && !simulationEnded && it !== null
	}
	
	/**
	 * Activates any kind of model element. The result depends on the type of
	 * the element. Typically the incoming transition / source element will be
	 * deactivated.
	 */
	def dispatch void activate(ModelElement it) {
		if (canActivate) {
			debugPush('Activation of')
			active = true
			debugPop
		}
	}

	/**
	 * Activates a transition. The source element will be deactivated.
	 */
	def dispatch void activate(Transition it) {
		if (canActivate) {
			debugPush('Activation of')
			sourceElement.deactivate
			active = true
			debugPop
		}
	}
	
	/**
	 * Activates a state node and its parallel lanes. The active incoming
	 * transitions will be deactivated.
	 */
	def dispatch void activate(State it) {
		if (canActivate) {
			debugPush('Activation of')
			incomingTransitions.forEach[deactivate]
			active = true
			parallelLanes.forEach[activate]
			if (endsAreActive) {
				outgoingStateTransitions.forEach[doTrigger]
			}
			debugPop
		}
	}
	
	/**
	 * Activates a parallel lane node and its start node.
	 */
	def dispatch void activate(ParallelLane it) {
		if (canActivate) {
			debugPush('Activation of')
			active = true
			start.activate
			debugPop
		}
	}
	
	/**
	 * Activates the outgoing simple transition of the start node.
	 */
	def dispatch void activate(Start it) {
		if (canActivate) {
			debugPush('Activation of')
			active = true
			outgoingSimpleTransition.doTrigger
			debugPop
		}
	}
	
	/**
	 * Activates an end node. If all end nodes of the parent state are active,
	 * The first transition with a default trigger will be activated.
	 */
	def dispatch void activate(End it) {
		if (canActivate) {
			debugPush('Activation of')
			incomingTransitions.forEach[deactivate]
			active = true
			if (parentState.endsAreActive) {
				parentState.outgoingStateTransitions.filter[trigger == defaultTrigger].forEach[doTrigger]
			}
			debugPop
		}
	}
	
	/**
	 * Activates one of its outgoing transitions.
	 */
	def dispatch void activate(BranchAndMerge it) {
		if (canActivate) {
			debugPush('Activation of')
			active = true
			if (incomingTransitions.exists[active]) {
				incomingTransitions.forEach[deactivate]
				outgoingGuardedTransitions.forEach[doTrigger]
			}
			debugPop
		}
	}
	
	/**
	 * Restores the previously active elements, or activates its outgoing
	 * transition, if there was no previous history.
	 */
	def dispatch void activate(NormalHistory it) {
		if (canActivate) {
			debugPush('Activation of')
			incomingTransitions.forEach[deactivate]
			active = true
			val history = histories.get(it)
			if (history === null || history.empty) {
				// If this history node has no former simulation state,
				// then follow the default transition.
				outgoingSimpleTransition.doTrigger
			}
			else {
				// Else, set activation state for each element from history.
				// First, set every element of the (parent) parallel lane to inactive.
				parentNode.descendantElements.forEach[active = false]
				// Then, activate all active elements from the history.
				history.forEach[element, value | if (value) element.activate]
			}
			debugPop
		}
	}
	
	/**
	 * Restores the previously active elements, or activates its outgoing
	 * transition, if there was no previous history.
	 */
	def dispatch void activate(AtomicHistory it) {
		if (canActivate) {
			debugPush('Activation of')
			incomingTransitions.forEach[deactivate]
			active = true
			val history = histories.get(it)
			if (history === null || history.empty) {
				// If this history node has no former simulation state,
				// then follow the default transition.
				outgoingSimpleTransition.doTrigger
			}
			else {
				// Else, set activation state for each element from history.
				// Do not use 'activate'! Avoid automatic activation of elements
				// by setting the active state directly.
				history.forEach[element, value | element.active = value]
			}
			debugPop
		}
	}
	
	/**
	 * If the model element is {@code null}, do nothing. (This is a workaround.
	 * If this dispatch case prevents a {@link IllegalArgumentException} when
	 * calling {@code activate(null)}.)
	 */
	def dispatch void activate(Void it) {
		// Intentionally left blank
	}
	
	
	
	/* XXX: Deactivate ************************************************************/
	
	
	
	/**
	 * Checks, if a model element can be deactivated or not.
	 */
	def boolean canDeactivate(ModelElement it) {
		return initialized && !simulationEnded && it !== null
	}
	
	/**
	 * Deactivates any kind of model element. The result depends on the type of the element.
	 */
	def dispatch void deactivate(ModelElement it) {
		if (canDeactivate) {
			debugPush('Deactivation of')
			active = false
			debugPop
		}
	}
	
	/**
	 * Deactivates a state and all its containing parallel lanes.
	 */
	def dispatch void deactivate(State it) {
		if (canDeactivate) {
			debugPush('Deactivation of')
			active = false
			parallelLanes.forEach[deactivate]
			debugPop
		}
	}
	
	/**
	 * Deactivates a lane and all its containing elements.
	 * Before deactivation, the activation state of the containing elements will
	 * be saved (if a history node is present).
	 */
	def dispatch void deactivate(ParallelLane it) {
		
		if (canDeactivate) {
			
			debugPush('Deactivation of')
			
			if (parentState.endsAreActive) {
				for (lane: parentState.parallelLanes) {
					histories.remove(lane.normalHistory)
					histories.remove(lane.atomicHistory)
				}
			}
			else {
				// If the lane has a normal history, save the activation states of all child elements.
				if (hasNormalHistory) {
					normalHistory.debugPush('Saving to')
					val HashMap<ModelElement, Boolean> elements = newHashMap
					val childElements = childElements
					childElements.forEach[
						elements.put(it, active)
					]
					histories.put(normalHistory, elements)
					normalHistory.debugPop
				}
				
				// If the lane has an atomic history, save the activation states of all descendant elements.
				if (hasAtomicHistory) {
					atomicHistory.debugPush('Saving to')
					val HashMap<ModelElement, Boolean> elements = newHashMap
					val descendantElements = descendantElements
					descendantElements.forEach[
						elements.put(it, active)
					]
					histories.put(atomicHistory, elements)
					atomicHistory.debugPop
				}
			}
			
			// Deactivate the lane and each child element
			active = false
			childElements.forEach[deactivate]
			debugPop
		}
		
	}
	
	/**
	 * If the model element is {@code null}, do nothing. (This is a workaround.
	 * This dispatch case prevents an {@link IllegalArgumentException} when
	 * calling {@code deactivate(null)}.)
	 */
	def dispatch void deactivate(Void it) {
		// Intentionally left blank
	}
	
	
	
	/* XXX: Evaluation ************************************************************/
	
	
	
	/**
	 * Checks if state transitions with a default trigger can be activated. If
	 * true the simulation will advance and {@code true} will be returned.
	 */
	def void updateGuards() {		
		for (transition: stateChart.find(Transition)) {
			if (transition.hasGuard) {
				transition.doTrigger
			}
		}
	}
	
	/**
	 * Checks if state transitions with a default trigger can be activated and
	 * a guard containing the provided variable name. If true the simulation
	 * will advance and {@code true} will be returned.
	 */
	def void updateGuards(VariableDeclaration it) {		
		for (transition: stateChart.find(Transition)) {
			if (transition.hasGuard   && transition.guard.contains(it.label) &&
				transition.hasTrigger && transition.trigger == defaultTrigger) {
				transition.doTrigger
			}
		}
	}
	
	/**
	 * Runs the transition's action, if it has one.
	 */
	def void doAction(Transition it) {
		if (hasAction) action.evaluateAction
	}
	
	
	
	/* XXX: Local Extension Methods ***********************************************/
	
	
	
	/**
	 * Returns {@code true} if the parallel lane has at least one end node,
	 * that is active.
	 */
	def boolean hasActiveEnd(ParallelLane it) {
		return ends.exists[active]
	}
	
	/**
	 * Returns {@code true}, if all parallel lanes have at least one active end
	 * or the state is simple.
	 */
	def boolean endsAreActive(State it) {
		return it !== null && (simple || parallelLanes.forall[hasActiveEnd])
	}
	
	
	
	/* XXX: Methods ***************************************************************/
	
	
	
	/**
	 * Activates or deactivates model element highlighting. The highlight color
	 * depends on the type of model element.
	 */
	def boolean setHighlight(ModelElement it, boolean value) {
		switch (it) {
			State case  simple:                              backgroundColor = 'FF8C69' // Salmon (Red)
			State case complex:                              backgroundColor = 'FFB1B3' // Sundown (Pink)
			ParallelLane case nodes.empty: foregroundColor = backgroundColor = 'FF8C69' // Salmon (Red)
			ParallelLane:                                    backgroundColor = 'FFFFFF' // White
			End:                           foregroundColor = backgroundColor = '32CD32' // Limegreen (Green)
			Transition:                    foregroundColor =                   '32CD32' // Limegreen (Green)
			case debug:                                      backgroundColor = '0000FF' // Blue
			default:                                         backgroundColor = 'FFFFFF' // White
		}
		highlighted = value
		return value
	}
	
	/**
	 * Refreshes all highlights.
	 */
	def void refreshHighlights() {
		removeAllHighlights
		activeElements.forEach[highlight = true]
	}
	
	/**
	 * Refreshes all views of the simulator and brings the currently simulated
	 * state chart to the front.
	 */
	def void refresh() {
		if (initialized) {
			stateChart.show
			if(stateChart === activeStateChart) {
				refreshHighlights
				triggerView.refresh
				variableView.refresh
			}
			else {
				deinitialize
			}
		}
	}
	
	/**
	 * Returns a {@code boolean} whether the simulator was initialized or not.
	 */
	def boolean isInitialized() {
		return stateChart !== null
	}
	
	/**
	 * Returns {@code true}, if the simulation ended. Returns {@code false}
	 * otherwise, or if the simulator was not {@link #isInitialized()
	 * initialized}. The simulation ends, if at least one end node of the
	 * state chart itself is active.
	 */
	def boolean simulationEnded() {
		return initialized && stateChart.ends.exists[active]
	}
	
	/**
	 * Deinitializes the simulator.
	 */
	def void deinitialize() {
		setEnabled(false)
		stateChart = null
		triggerView = null
		variableView = null
		debugStack = null
		removeAllHighlights
	}
	
	/** Wheather or not the trigger and variable views are enabled. */
	def boolean isEnabled() {
		return enabled
	}
	
	/** Sets the enabled state of the trigger and variable views. */
	def boolean setEnabled(boolean value) {
		if (initialized && triggerView !== null && variableView !== null) {
			enabled = value
			triggerView.enabled = value
			variableView.enabled = value
		}
		return value
	}
	
	
	
	/* XXX: Debugging ********************************************************/
	
	
	
	/**
	 * Returns a representative name for an Object. (Debugging purposes only!)
	 */
	def String debugName(Object it) {
		switch (it) {
			StateChart:                              '''StateChart "«name»"'''
			State:                                   '''State "«label»"'''
			ParallelLane:                            '''Lane «parentState.parallelLanes.indexOf(it)» of «parentState.debugName»'''
			Start case parentNode === null:          '''Start of StateChart'''
			Start case parentNode !== null:          '''Start of «parentNode.debugName»'''
			End case parentNode === null:            '''End of StateChart'''
			End case parentNode !== null:            '''End of «parentNode.debugName»'''
			BranchAndMerge case parentNode === null: '''BranchAndMerge of StateChart'''
			BranchAndMerge case parentNode !== null: '''BranchAndMerge of «parentNode.debugName»'''
			NormalHistory:                           '''NormalHistory of «parentNode.debugName»'''
			AtomicHistory:                           '''AtomicHistory of «parentNode.debugName»'''
			Transition case !hasTrigger:             '''(«sourceElement.debugName») --> («targetElement.debugName»)'''
			Transition case hasTrigger:              '''(«sourceElement.debugName») -«trigger»-> («targetElement.debugName»)'''
			case null:                               'null'
			default:                                 class.simpleName
		}
	}
	
	/**
	 * Returns indentation whitespace corresponding to the depth of the
	 * {@link #debugStack debugStack} for indenting the debug text.
	 * (Debugging purposes only!)
	 */
	def String debugIndentation() {
		if (debug) {
			var t = 'Simulator Debug:\t'
			for (i: 0 ..< debugStack.size) {
				t += '  '
			}
			return t
		}
		else {
			return ''
		}
	}
	
	/**
	 * Pushes an action to the {@link #debugStack debugStack}.
	 * (Debugging purposes only!)
	 */
	def void debugPush(Object it, String message) {
		if (debug) {
			System.out.println('''«debugIndentation»«message.trim» «if (it !== null) debugName»''')
			debugStack.push(it)
		}
	}
	
	/**
	 * Pops an action from the {@link #debugStack debugStack}.
	 * (Debugging purposes only!)
	 */
	def void debugPop(Object it) {
		if (debug) {
			if (debugStack.peek === it) {
				debugStack.pop
			}
			else {
				System.err.println(debugIndentation + 'ERROR: Top element of debug stack did not match!')
			}
		}
	}
	
}
