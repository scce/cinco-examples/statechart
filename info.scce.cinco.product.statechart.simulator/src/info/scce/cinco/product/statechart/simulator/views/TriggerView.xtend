package info.scce.cinco.product.statechart.simulator.views

import info.scce.cinco.product.statechart.simulator.Simulator
import java.util.HashMap
import java.util.List
import org.eclipse.jface.action.Action
import org.eclipse.swt.SWT
import org.eclipse.swt.events.ControlEvent
import org.eclipse.swt.events.ControlListener
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.part.ViewPart

import static extension info.scce.cinco.product.statechart.extensions.StateChartExtension.*
import static extension info.scce.cinco.product.statechart.simulator.extensions.IconExtension.*

class TriggerView extends ViewPart {

	public static final String id = 'info.scce.cinco.product.statechart.simulator.views.TriggerView'

	Composite container
	HashMap<String, Button> buttons = newHashMap
	Action refreshAction
	boolean buttonsEnabled

	override createPartControl(Composite parent) {
		
		buttonsEnabled = true
		container = parent
		
		refreshAction = new Action('Refresh', Action.AS_PUSH_BUTTON) {
			override run() { refresh }
		}
		refreshAction.imageDescriptor = getImageDescriptor('Refresh')
		viewSite.actionBars.toolBarManager.add(refreshAction)
		
        // Add listener for automatic layouting after resize
		container.addControlListener(new ControlListener {
			override controlMoved(ControlEvent e) {
				// Intentionally left blank
			}
			override controlResized(ControlEvent e) {
				layout
			}
		})
		
		refresh
		
	}

	override setFocus() {
		// Intentionally left blank
	}
	
	/**
	 * Shows the view in the current workbench window.
	 */
	static def show() {
		val triggerView = PlatformUI.workbench.activeWorkbenchWindow.activePage.showView(id) as TriggerView
		triggerView.refresh
		return triggerView
	}

	/**
	 * Adds a button. If a button with the same text already exists, it will be
	 * removed and a new button will be added. The button may not appear in the
	 * view until {@link #layout() layout()} is called.
	 */
	def addButton(String buttonText) {
		removeButton(buttonText)
		val button = new Button(container, SWT.NONE)
		button.layoutData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1)
		button.text = buttonText
		button.enabled = buttonsEnabled
		button.addListener(SWT.Selection) [
			Simulator.instance.doTrigger(buttonText)
		]
		buttons.put(buttonText, button)
	}

	/**
	 * Adds buttons with the provided button texts. If a button with the same
	 * text already exists, it will be removed and a new button will be added.
	 * The buttons may not appear in the view until {@link #layout() layout()}
	 * is called.
	 */
	def addAllButtons(List<String> buttonTexts) {
		buttonTexts.sort.forEach[addButton]
	}

	/**
	 * Removes the button with the corresponding text. The button may still
	 * appear in the view until {@link #layout() layout()} is called.
	 */
	def removeButton(String buttonText) {
		val button = buttons.remove(buttonText)
		if(button !== null) button.dispose
	}

	/**
	 * Removes all buttons. The buttons may still appear in the view until
	 * {@link #layout() layout()} is called.
	 */
	def removeAllButtons() {
		container.children.filter[it instanceof Button].forEach[dispose]
		buttons.clear
	}

	/**
	 * Refreshes the view. Removes all buttons and adds one for each trigger of
	 * the currently simulated state chart.
	 */
	def refresh() {
		removeAllButtons
		if (Simulator.instance.initialized) {
			val triggers = Simulator.instance.stateChart.triggerLabels
			addAllButtons(triggers)
		}
		layout
	}

	/**
	 * Sets the layout of the view and <i>lays</i> it <i>out</i>. It sets the
	 * number of columns, such that each button is between 100 and 199 pt wide.
	 */
	def layout() {
		val columns = container.size.x / 100
		container.layout = new GridLayout(columns, true)
		container.layout()
	}
	
	/**
	 * Whether the buttons of the trigger view are enabled or not.
	 */
	def boolean isEnabled() {
		return buttonsEnabled
	}
	
	/**
	 * En- or disables all buttons of the trigger view.
	 */
	def boolean setEnabled(boolean value) {
		buttonsEnabled = value
		buttons.values.forEach[enabled = value]
		refreshAction.enabled = value
		return value
	}

}
