package info.scce.cinco.product.statechart.simulator.views

import info.scce.cinco.product.statechart.simulator.Simulator
import org.eclipse.jface.action.Action
import org.eclipse.jface.viewers.ArrayContentProvider
import org.eclipse.jface.viewers.ColumnLabelProvider
import org.eclipse.jface.viewers.EditingSupport
import org.eclipse.jface.viewers.TableViewer
import org.eclipse.jface.viewers.TableViewerColumn
import org.eclipse.jface.viewers.TextCellEditor
import org.eclipse.swt.SWT
import org.eclipse.swt.events.ControlEvent
import org.eclipse.swt.events.ControlListener
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.part.ViewPart

import static extension info.scce.cinco.product.statechart.extensions.StateChartExtension.*
import static extension info.scce.cinco.product.statechart.simulator.extensions.IconExtension.*
import info.scce.cinco.product.statechart.statechart.VariableDeclaration
import info.scce.cinco.product.statechart.statechart.BooleanDeclaration
import info.scce.cinco.product.statechart.statechart.IntegerDeclaration

class VariableView extends ViewPart {
	
	public static final String id = 'info.scce.cinco.product.statechart.simulator.views.VariableView'

	Composite container
	TableViewer tableViewer
	TableViewerColumn typeColumn
	TableViewerColumn nameColumn
	TableViewerColumn valueColumn
	Action refreshAction
	boolean tableEnabled

	override createPartControl(Composite parent) {
		
		tableEnabled = true
		container = parent
		tableViewer = new TableViewer(container, or(SWT.MULTI, SWT.H_SCROLL, SWT.V_SCROLL, SWT.BORDER, SWT.FULL_SELECTION))
		tableViewer.cellEditors
		
		// Refresh action
		refreshAction = new Action('Refresh', Action.AS_PUSH_BUTTON) {
			override run() { refresh }
		}
		refreshAction.imageDescriptor = getImageDescriptor('Refresh')
		viewSite.actionBars.toolBarManager.add(refreshAction)
		
		// Type column
		typeColumn = new TableViewerColumn(tableViewer, SWT.NONE)
		typeColumn.column.text = 'Type'
		typeColumn.column.width = 100
		typeColumn.column.resizable = true
		typeColumn.column.moveable = false
		
		typeColumn.labelProvider = new ColumnLabelProvider {
			override getText(Object element) {
				switch element {
					BooleanDeclaration: 'Boolean'
					IntegerDeclaration: 'Integer'
					default:            'ERROR'
				}
			}
		}
		
		// Name column
		nameColumn = new TableViewerColumn(tableViewer, SWT.NONE)
		nameColumn.column.text = 'Name'
		nameColumn.column.width = 100
		nameColumn.column.resizable = true
		nameColumn.column.moveable = false
		
		nameColumn.labelProvider = new ColumnLabelProvider {
			override getText(Object element) {
				val variable = element as VariableDeclaration
				return variable.label
			}
		}
		
		// Value column
		valueColumn = new TableViewerColumn(tableViewer, SWT.NONE)
		valueColumn.column.text = 'Value'
		valueColumn.column.width = 100
		valueColumn.column.resizable = true
		valueColumn.column.moveable = false
		
		valueColumn.labelProvider = new ColumnLabelProvider {
			override getText(Object element) {
				val extension simulator = Simulator.instance
				switch element {
					BooleanDeclaration: element.value.toString
					IntegerDeclaration: element.value.toString
					default:            'ERROR'
				}
			}
		}
		
		valueColumn.editingSupport = new EditingSupport(tableViewer) {
			
			override protected canEdit(Object element) {
				return true
			}
			
			override protected getCellEditor(Object element) {
				return new TextCellEditor(tableViewer.table)
			}
			
			override protected getValue(Object element) {
				val extension simulator = Simulator.instance
				switch element {
					BooleanDeclaration: element.value.toString
					IntegerDeclaration: element.value.toString
					default:            'ERROR'
				}
			}
			
			override protected setValue(Object element, Object value) {
				val stringValue = value.toString.trim.toLowerCase
				val extension simulator = Simulator.instance
				switch element {
					BooleanDeclaration: {
						if (#['true', 'yes', 'on', '1'].contains(stringValue)) {
							element.value = true
						}
						else if (#['false', 'no', 'off', '0'].contains(stringValue)) {
							element.value = false
						}
					}
					IntegerDeclaration: {
						try {
							element.value = Integer.parseInt(stringValue)
						}
						catch(NumberFormatException e) {
							// Intentionally left blank
						}
					}
				}
			}
			
		}
			
		// Table
		tableViewer.table.headerVisible = true
		tableViewer.table.linesVisible = true
		
		// Content provider
		tableViewer.contentProvider = ArrayContentProvider.instance
		refresh
		
		// Make the selection available to other views
        site.selectionProvider = tableViewer
        
        // Layout the viewer
        val gridData = new GridData
        gridData.horizontalAlignment = GridData.FILL
        gridData.verticalAlignment = GridData.FILL
        gridData.grabExcessHorizontalSpace = true
        gridData.grabExcessVerticalSpace = true
        gridData.horizontalSpan = 2
        tableViewer.control.layoutData = gridData
        
        // Add listener for automatic column resizing after resizing the table
        tableViewer.table.addControlListener(new ControlListener {
			override controlMoved(ControlEvent e) {
				// Intentionally left blank
			}
			override controlResized(ControlEvent e) {
				layout
			}
		})
		
		// Add listener for automatic column resizing after resizing the name column
        typeColumn.column.addControlListener(new ControlListener {
			override controlMoved(ControlEvent e) {
				// Intentionally left blank
			}
			override controlResized(ControlEvent e) {
				layoutValueColumn
			}
		})
		
		// Add listener for automatic column resizing after resizing the name column
        nameColumn.column.addControlListener(new ControlListener {
			override controlMoved(ControlEvent e) {
				// Intentionally left blank
			}
			override controlResized(ControlEvent e) {
				layoutValueColumn
			}
		})
		
		// Add listener for automatic column resizing after resizing the value column
        valueColumn.column.addControlListener(new ControlListener {
			override controlMoved(ControlEvent e) {
				// Intentionally left blank
			}
			override controlResized(ControlEvent e) {
				// Uncommenting may cause stack overflow when creating a new VariableView:
				// layoutValueColumn
			}
		})
        
        refresh
        
	}

	override setFocus() {
		// Passing the focus request to the viewer's control
		tableViewer.control.setFocus
	}
	
	/**
	 * Shows the view in the current workbench window.
	 */
	static def show() {
		val variableView = PlatformUI.workbench.activeWorkbenchWindow.activePage.showView(id) as VariableView
		variableView.refresh
		return variableView
	}
	
	/**
	 * Refreshes the values of the table.
	 */
	def refresh() {
		tableViewer.input = if (Simulator.instance.initialized) Simulator.instance.stateChart.variableDeclarations else null
		tableViewer.refresh(true, true)
		layout
	}
	
	/**
	 * Resizes the columns, so they fill the available width. The ratio between
	 * the columns will stay the same.
	 */
	def layout() {
		val typeWidth      = typeColumn.column.width as double
		val nameWidth      = nameColumn.column.width as double
		val valueWidth     = valueColumn.column.width as double
		val totalWidth     = typeWidth + nameWidth + valueWidth
		val availableWidth = tableViewer.table.clientArea.width as double
		if (totalWidth == 0) {
			typeColumn.column.width  = 100
			nameColumn.column.width  = 100
			valueColumn.column.width = 100
		}
		else {
			typeColumn.column.width  = (typeWidth  / totalWidth * availableWidth) as int
			nameColumn.column.width  = (nameWidth  / totalWidth * availableWidth) as int
			valueColumn.column.width = (valueWidth / totalWidth * availableWidth) as int
		}
	}
	
	/**
	 * Resizes the value column, so both columns fill the available width.
	 */
	def layoutValueColumn() {
		val typeWidth = typeColumn.column.width
		val nameWidth = nameColumn.column.width		
		val availableWidth = tableViewer.table.clientArea.width
		valueColumn.column.width = availableWidth - typeWidth - nameWidth
	}
	
	/**
	 * Whether the variable view is enabled or not.
	 */
	def boolean isEnabled() {
		return tableEnabled
	}
	
	/**
	 * En- or disables variable view.
	 */
	def boolean setEnabled(boolean value) {
		tableEnabled = value
		tableViewer.table.enabled = value
		refreshAction.enabled = value
		return value
	}
	
	/**
	 * Bitwise "or" of all elements of {@code x}.
	 */
	def or(int... x) {
		return x.fold(0)[int a, int b | a.bitwiseOr(b)]
	}
	
}