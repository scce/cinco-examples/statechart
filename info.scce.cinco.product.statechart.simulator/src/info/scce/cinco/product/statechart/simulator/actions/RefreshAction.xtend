package info.scce.cinco.product.statechart.simulator.actions

import info.scce.cinco.product.statechart.simulator.Simulator
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException

class RefreshAction extends AbstractHandler {
	
	override Object execute(ExecutionEvent event) throws ExecutionException {
		
		Simulator.instance.refresh
		return null
		
	}

}
