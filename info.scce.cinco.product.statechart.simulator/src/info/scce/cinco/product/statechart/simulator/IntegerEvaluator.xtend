package info.scce.cinco.product.statechart.simulator

import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AddSubExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AddTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AndExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AtomicArithmeticExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AtomicBooleanExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.DivTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MinusExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MulDivExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MulTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NandExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NorExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NotExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.OrExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.PowExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.RelationalExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.SubTerm
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.XnorExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.XorExpression
import info.scce.cinco.product.statechart.statechart.IntegerDeclaration

import static extension info.scce.cinco.product.statechart.dsl.guard.extensions.GuardExpressionExtension.*
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.GuardExpression

class IntegerEvaluator {
	
	/**
	 * Evaluates a guard. The guard is a string formatted in the {@link
	 * info.scce.cinco.product.statechart.dsl.guard.guardExpression.GuardExpression
	 * GuardExpression} language (guard). Returns the evaluated value of the
	 * expression or {@code 0} if the string is {@code null}, empty or the
	 * default guard.
	 */
	static def dispatch int evaluateInteger(String code) {
		if (code.nullOrEmpty) return 0
		code.parseGuard.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(Void it) {
		return 0
	}
	
	static def dispatch int evaluateInteger(GuardExpression it) {
		if (defaultGuard || elseGuard) return 0
		if (isArithmeticExpression) return term.evaluateInteger
		return 0
	}
	
	static def dispatch int evaluateInteger(OrExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The or expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(NorExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The nor expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(AndExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The and expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(NandExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The nand expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(XorExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The xor expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(XnorExpression it) {
		if (!right.nullOrEmpty) throw new IllegalArgumentException('The xnor expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(NotExpression it) {
		if (not) throw new IllegalArgumentException('The not expression cannot be evaluated to an integer, because it has a not.')
		return term.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(AtomicBooleanExpression it) {
		if (literal !== null) throw new IllegalArgumentException('The atomic boolean expression cannot be evaluated to an integer, because it is a literal boolean.')
		return term.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(RelationalExpression it) {
		if (right !== null) throw new IllegalArgumentException('The relational expression cannot be evaluated to an integer, because the right side is not null.')
		return left.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(AddSubExpression it) {
		return right.fold(left.evaluateInteger) [l, r |
			switch r {
				AddTerm: l + r.term.evaluateInteger
				SubTerm: l - r.term.evaluateInteger
				default: throw new IllegalArgumentException('''The add/sub expression cannot be evaluated to an integer, because the type of the right side "«right.class.name»" is unknown.''')
			}
		]
	}
	
	static def dispatch int evaluateInteger(MulDivExpression it) {
		return right.fold(left.evaluateInteger) [l, r |
			switch r {
				MulTerm: l * r.term.evaluateInteger
				DivTerm: l / r.term.evaluateInteger
				default: throw new IllegalArgumentException('''The mul/div expression cannot be evaluated to an integer, because the type of the right side "«right.class.name»" is unknown.''')
			}
		]
	}
	
	static def dispatch int evaluateInteger(PowExpression it) {
		if (right === null) return left.evaluateInteger
		return Math.pow(left.evaluateInteger, right.evaluateInteger) as int
	}
	
	static def dispatch int evaluateInteger(MinusExpression it) {
		return if (minus) -term.evaluateInteger else term.evaluateInteger
	}
	
	static def dispatch int evaluateInteger(AtomicArithmeticExpression it) {
		if (term !== null) {
			return term.evaluateInteger
		}
		else if (reference instanceof IntegerDeclaration) {
			val extension simulator = Simulator.instance
			return (reference as IntegerDeclaration).value
		}
		else if (reference !== null) {
			throw new IllegalArgumentException('The atomic arithmetic expression cannot be evaluated to an integer, because it is reference to a boolean.')
		}
		else {
			return literal
		}
	}
	
}