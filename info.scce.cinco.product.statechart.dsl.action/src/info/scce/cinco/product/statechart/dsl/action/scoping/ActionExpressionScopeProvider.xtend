package info.scce.cinco.product.statechart.dsl.action.scoping

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes

import static extension info.scce.cinco.product.statechart.extensions.StateChartExtension.*

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class ActionExpressionScopeProvider extends AbstractActionExpressionScopeProvider {
	
	override getScope(EObject context, EReference reference) {
				
		val stateChart = activeStateChart
		if (stateChart !== null) {
			val decl = stateChart.variableDeclarations
			val scope = Scopes.scopeFor(decl, [QualifiedName.create(label)], IScope.NULLSCOPE)
			return scope
		}
		else {
			super.getScope(context, reference)
		}
	}
	
}
