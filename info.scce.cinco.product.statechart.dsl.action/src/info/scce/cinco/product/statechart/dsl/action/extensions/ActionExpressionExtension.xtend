package info.scce.cinco.product.statechart.dsl.action.extensions

import info.scce.cinco.product.statechart.dsl.action.ActionExpressionStandaloneSetup
import info.scce.cinco.product.statechart.dsl.action.actionExpression.ActionExpression
import java.io.ByteArrayInputStream
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.mwe.utils.StandaloneSetup
import org.eclipse.xtext.resource.XtextResourceSet

import static info.scce.cinco.product.statechart.extensions.TransitionExtension.*

class ActionExpressionExtension {
	
	static def Resource getActionExpressionResource(String code) {
		if (code === null) {
			return null
		}
		new StandaloneSetup().setPlatformUri("../")
		val injector = (new ActionExpressionStandaloneSetup).createInjectorAndDoEMFRegistration
		val resourceSet = injector.getInstance(XtextResourceSet)
		val resource = resourceSet.createResource(URI.createURI("action:/action.action"))
		val in = new ByteArrayInputStream((code ?: defaultAction).bytes)
		resource.load(in, resourceSet.loadOptions)
		return resource
	}
	
	static def ActionExpression parseAction(String code) {
		val resource = code.actionExpressionResource
		resource.errors.forEach[
			System.err.println('''ActionExpression: Error in line «line»: "«code.split('''\n''').get(line - 1)»": «message»''')
		]
		val action = resource.contents.head
		if (action instanceof ActionExpression) {
			return action
		}
		else {
			throw new IllegalArgumentException('''The action expression "«code»" could not be parsed.''')
		}
	}
	
	static def boolean isDefaultAction(ActionExpression it) {
		return noAction || instructions.nullOrEmpty
	}
	
}