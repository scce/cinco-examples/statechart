package info.scce.cinco.product.statechart.dsl.action.validation

import info.scce.cinco.product.statechart.dsl.action.actionExpression.Assignment
import info.scce.cinco.product.statechart.statechart.BooleanDeclaration
import info.scce.cinco.product.statechart.statechart.IntegerDeclaration
import info.scce.cinco.product.statechart.dsl.action.actionExpression.ActionExpressionPackage
import org.eclipse.xtext.validation.Check

import static extension info.scce.cinco.product.statechart.dsl.guard.extensions.GuardExpressionExtension.*

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class ActionExpressionValidator extends AbstractActionExpressionValidator {
	
	@Check
	def typeMismatch(Assignment it) {
		if (   (reference instanceof BooleanDeclaration && term.isArithmeticExpression)
			|| (reference instanceof IntegerDeclaration && term.isBooleanExpression   )) {
			typeMismatchError
		}
	}
	
	def typeMismatchError(Assignment it) {
		val title = 'Type mismatch'
		val message = '''Variable «reference.label» («reference.class.name») is incompatible with this term.'''
		error('''«title»: «message»''', it, ActionExpressionPackage.Literals.ASSIGNMENT__TERM, title)
	}
	
}
