package info.scce.cinco.product.statechart.dsl.guard.validation

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class GuardExpressionValidator extends AbstractGuardExpressionValidator {
	
//	@Check
//	def typeMismatch(Assignment it) {
//		if (   reference.type == Type.BOOLEAN && term.isArithmeticExpression
//			|| reference.type == Type.INT     && term.isBooleanExpression   ) {
//			typeMismatchError
//		}
//	}
//	
//	def typeMismatchError(Assignment it) {
//		val title = 'Type mismatch'
//		val message = '''Variable «reference.name» («reference.type») is incompatible with this expresion.'''
//		error('''«title»: «message»''', it, BooleanExpressionPackage.Literals.ASSIGNMENT__TERM, title)
//	}
	
}
