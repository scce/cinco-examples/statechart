package info.scce.cinco.product.statechart.dsl.guard.extensions

import info.scce.cinco.product.statechart.dsl.guard.GuardExpressionStandaloneSetup
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AddSubExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AndExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AtomicArithmeticExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.AtomicBooleanExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.GuardExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MinusExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.MulDivExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NandExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NorExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.NotExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.OrExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.PowExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.RelationalExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.XnorExpression
import info.scce.cinco.product.statechart.dsl.guard.guardExpression.XorExpression
import info.scce.cinco.product.statechart.statechart.BooleanDeclaration
import info.scce.cinco.product.statechart.statechart.IntegerDeclaration
import java.io.ByteArrayInputStream
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.mwe.utils.StandaloneSetup
import org.eclipse.xtext.resource.XtextResourceSet

import static info.scce.cinco.product.statechart.extensions.TransitionExtension.*

class GuardExpressionExtension {
	
	def static Resource getResource(String code) {
		if (code === null) {
			return null
		}
		new StandaloneSetup().setPlatformUri("../")
		val injector = (new GuardExpressionStandaloneSetup).createInjectorAndDoEMFRegistration
		val resourceSet = injector.getInstance(XtextResourceSet)
		val resource = resourceSet.createResource(URI.createURI("guard:/guard.guard"))
		val in = new ByteArrayInputStream((code ?: defaultGuard).bytes)
		resource.load(in, resourceSet.loadOptions)
		return resource
	}
	
	def static GuardExpression parseGuard(String code) {
		val resource = code.resource
		resource.errors.forEach[
			System.err.println('''GuardExpression: Error in line «line»: «location»: «message»''')
		]
		val guard = resource.contents.head
		if (guard instanceof GuardExpression) {
			return guard
		}
		else {
			throw new IllegalArgumentException('''The guard expression "«code»" could not be parsed.''')
		}
	}
	
	def static boolean isDefaultGuard(GuardExpression it) {
		return noGuard || (term === null && !it.elseGuard)
	}
	
	
	
	/*** Is boolean expression ***/
	
	def static dispatch boolean isBooleanExpression(GuardExpression it) {
		if (term !== null)		return term.isBooleanExpression
		false
	}
	
	
	
	def static dispatch boolean isBooleanExpression(OrExpression it) {
		left.isBooleanExpression &&
		(right.nullOrEmpty || right.forall[isBooleanExpression])
	}
	
	def static dispatch boolean isBooleanExpression(NorExpression it) {
		left.isBooleanExpression &&
		(right.nullOrEmpty || right.forall[isBooleanExpression])
	}
	
	def static dispatch boolean isBooleanExpression(AndExpression it) {
		left.isBooleanExpression &&
		(right.nullOrEmpty || right.forall[isBooleanExpression])
	}
	
	def static dispatch boolean isBooleanExpression(NandExpression it) {
		left.isBooleanExpression &&
		(right.nullOrEmpty || right.forall[isBooleanExpression])
	}
	
	def static dispatch boolean isBooleanExpression(XorExpression it) {
		left.isBooleanExpression &&
		(right.nullOrEmpty || right.forall[isBooleanExpression])
	}
	
	def static dispatch boolean isBooleanExpression(XnorExpression it) {
		left.isBooleanExpression &&
		(right.nullOrEmpty || right.forall[isBooleanExpression])
	}
	
	def static dispatch boolean isBooleanExpression(NotExpression it) {
		term.isBooleanExpression
	}
	
	def static dispatch boolean isBooleanExpression(AtomicBooleanExpression it) {
		if (literal !== null)	return true
		if (term !== null)		return term.isBooleanExpression
		false
	}
	
	
	
	def static dispatch boolean isBooleanExpression(RelationalExpression it) {
		(left.isBooleanExpression && right === null) ||
		(left.isArithmeticExpression && right !== null && right.term.isArithmeticExpression)
	}
	
	
	
	def static dispatch boolean isBooleanExpression(AddSubExpression it) {
		left.isBooleanExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isBooleanExpression(MulDivExpression it) {
		left.isBooleanExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isBooleanExpression(PowExpression it) {
		left.isBooleanExpression && right === null
	}
	
	def static dispatch boolean isBooleanExpression(MinusExpression it) {
		term.isBooleanExpression && !minus
	}	
	
	def static dispatch boolean isBooleanExpression(AtomicArithmeticExpression it) {
		if (term !== null)		return term.isBooleanExpression
		if (reference !== null)	return reference instanceof BooleanDeclaration
		false
	}
	
	
	
	def static dispatch boolean isBooleanExpression(EObject it) {
		false
	}
	
	def static dispatch boolean isBooleanExpression(Void it) {
		false
	}
	
	
	
	/*** Is arithmetic expression ***/
	
	def static dispatch boolean isArithmeticExpression(GuardExpression it) {
		if (term !== null)		return term.isArithmeticExpression
		false
	}
	
	
	
	def static dispatch boolean isArithmeticExpression(OrExpression it) {
		left.isArithmeticExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isArithmeticExpression(NorExpression it) {
		left.isArithmeticExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isArithmeticExpression(AndExpression it) {
		left.isArithmeticExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isArithmeticExpression(NandExpression it) {
		left.isArithmeticExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isArithmeticExpression(XorExpression it) {
		left.isArithmeticExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isArithmeticExpression(XnorExpression it) {
		left.isArithmeticExpression && right.nullOrEmpty
	}
	
	def static dispatch boolean isArithmeticExpression(NotExpression it) {
		term.isArithmeticExpression && !not
	}
	
	def static dispatch boolean isArithmeticExpression(AtomicBooleanExpression it) {
		if (literal !== null)	return false
		if (term !== null)		return term.isArithmeticExpression
		false
	}
	
	
	
	def static dispatch boolean isArithmeticExpression(RelationalExpression it) {
		left.isArithmeticExpression && right === null
	}
	
	
	
	def static dispatch boolean isArithmeticExpression(AddSubExpression it) {
		left.isArithmeticExpression &&
		(right.nullOrEmpty || right.forall[term.isArithmeticExpression])
	}
	
	def static dispatch boolean isArithmeticExpression(MulDivExpression it) {
		left.isArithmeticExpression &&
		(right.nullOrEmpty || right.forall[term.isArithmeticExpression])
	}
	
	def static dispatch boolean isArithmeticExpression(PowExpression it) {
		left.isArithmeticExpression &&
		(right === null || right.isArithmeticExpression)
	}
	
	def static dispatch boolean isArithmeticExpression(MinusExpression it) {
		term.isArithmeticExpression
	}	
	
	def static dispatch boolean isArithmeticExpression(AtomicArithmeticExpression it) {
		if (term !== null)		return term.isArithmeticExpression
		if (reference !== null)	return reference instanceof IntegerDeclaration
		true // it is a literal
	}
	
	
	
	def static dispatch boolean isArithmeticExpression(EObject it) {
		false
	}
	
	def static dispatch boolean isArithmeticExpression(Void it) {
		false
	}
	
}
