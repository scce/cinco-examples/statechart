/*** Appearances ***/

appearance solidAppearance {
	foreground (0, 0, 0)
	lineWidth 2
	lineStyle SOLID
}

appearance dashedAppearance {
	foreground (0, 0, 0)
	lineWidth 1
	lineStyle DASH
}

appearance shapeAppearance {
	background (255, 255, 255)
	foreground (0, 0, 0)
	lineWidth 1
	lineStyle SOLID
}

appearance filledShapeAppearance extends shapeAppearance {
	background (0, 0, 0)
}

appearance titleTextAppearance {
	foreground (0, 0, 0)
	font ("Helvetica", BOLD, 12)
}

appearance bodyTextAppearance {
	foreground (0, 0, 0)
	font ("Helvetica", 10)
}

appearance codeAppearance {
	foreground (0, 0, 0)
	font ("Monaco", 10)
}

appearance codeKeywordAppearance {
	foreground (127, 0, 85)
	font ("Monaco", BOLD, 10)
}



/*** Edge styles ***/

edgeStyle solidEdgeWithArrowAndText(1) {	
	appearance solidAppearance
	decorator arrow {
		// Put an arrow at the end of the edge
		location(1.0)
		ARROW
		appearance solidAppearance
	}
	decorator title {
		// Put text at the middle of the edge
		location(0.5)
		movable
		text {
			appearance codeAppearance
			value "%s"
		}
	}
}

edgeStyle dashedEdge {
	appearance dashedAppearance
}



/*** Node styles ***/

nodeStyle circle {
	ellipse circle {
		appearance shapeAppearance
		size (30, 30)
	}
}

nodeStyle filledCircle {
	ellipse circle {
		appearance filledShapeAppearance
		size (30, 30)
	}
}

nodeStyle circleContainingFilledCircle {
	ellipse outerCircle {
		appearance shapeAppearance
		size (30, 30)
		ellipse innerCircle {
			appearance filledShapeAppearance
			position (CENTER, MIDDLE)
			size (20, 20)
		}
	}
}

nodeStyle diamond {
	polygon box {
		appearance shapeAppearance
		points [(15, 0) (30, 15) (15, 30) (0, 15)]
		size (30, 30)
	}
}

nodeStyle circleContainingLetter(1) {
	ellipse circle {
		appearance shapeAppearance
		size (30, 30)
		text letter {
			appearance titleTextAppearance
			position (CENTER, MIDDLE)
			value "%s"
		}
	}
}

nodeStyle roundedRectangleWithTitle(1) {
	roundedRectangle box {
		appearance shapeAppearance
		size (100, 50)
		corner (25, 25)
		text title {
			appearance titleTextAppearance
			position (CENTER, TOP)
			value "%s"
		}
		polyline headerLine {
			appearance shapeAppearance
			points [(0, 20) (100, 20)]
			size (100, 1)
		}
	}
}

nodeStyle borderlessRoundedRectangle {
	roundedRectangle box {
		appearance extends shapeAppearance {
			foreground (255, 255, 255)
		}
		size (400, 100)
		corner (20, 20)
	}
}

nodeStyle dashedLine {
	rectangle box {
		appearance {
			transparency 1.0
		}
		size (200, 1)
		polyline line {
			appearance dashedAppearance
			points [(0, 0) (200, 0)]
			size (200, 1)
		}
	}
}

nodeStyle rectangleContainingText(1) {
	rectangle box {
		appearance shapeAppearance
		size (150, 100)
		text title {
			appearance titleTextAppearance
			position (CENTER, TOP 3)
			value "%s"
		}
	}
}

nodeStyle borderlessRectangleContainingText(2) {
	rectangle box {
		appearance extends shapeAppearance {
			foreground (255, 255, 255)
		}
		size (180, 20)
		text bodyLeft {
			appearance codeKeywordAppearance
			position (LEFT, MIDDLE)
			value "%1$s"
		}
		text bodyRight {
			appearance codeAppearance
			position (LEFT 65, MIDDLE)
			value "%2$s"
		}
	}
}

nodeStyle dashedRectangleContainingText(1) {
	rectangle box {
		appearance extends shapeAppearance {
			lineStyle DASH
			transparency 0.5
		}
		size (200, 600)
		text title {
			appearance titleTextAppearance
			position (CENTER, TOP 3)
			value "%s"
		}
	}
}

nodeStyle documentContainingText(1) {
	rectangle document {
		appearance extends shapeAppearance {
			background (255, 251, 217)
		}
		size (150, 100)
		polygon fold {
			appearance extends shapeAppearance {
				background (243, 239, 195)
			}
			position (RIGHT -1, TOP 1)
			points [(0, 0) (25, 25) (0, 25)]
			size (fix 25, fix 25)
		}
		multiText body {
			appearance bodyTextAppearance
			position (LEFT 5, TOP)
			value "%s"
		}
	}
}
