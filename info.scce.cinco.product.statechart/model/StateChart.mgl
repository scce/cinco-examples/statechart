id info.scce.cinco.product.statechart
stylePath "model/StateChart.style"

@mcam("check")
@mcam_checkmodule("info.scce.cinco.product.statechart.checks.BranchAndMergeCheck")
@mcam_checkmodule("info.scce.cinco.product.statechart.checks.DeclarationCheck")
@mcam_checkmodule("info.scce.cinco.product.statechart.checks.StateCheck")
@mcam_checkmodule("info.scce.cinco.product.statechart.checks.TransitionCheck")
graphModel StateChart {
	iconPath "icons/StateChartIcon.png"
	diagramExtension "statechart"
	containableElements(Start[1, 1], End, BranchAndMerge, State, SwimLane, Comment, DeclarationContainer[0, 1])
}

@palette("Declarations")
@event("info.scce.cinco.product.statechart.events.DeclarationContainerEvent")
container DeclarationContainer {
	style rectangleContainingText("Declarations")
	@propertiesViewHidden
	attr SortOrder as sortOrder := TYPE
	incomingEdges(Association)
	containableElements(TriggerDeclaration, BooleanDeclaration, IntegerDeclaration)
}

enum SortOrder {
	TYPE
	NAME
}

@event("info.scce.cinco.product.statechart.events.DeclarationEvent")
abstract node Declaration {
	@mcam_label
	attr EString as label := "<Missing label>"
	incomingEdges(Association)
}

abstract node VariableDeclaration extends Declaration {
	// Intentionally left blank
}

@palette("Declarations")
@disable(resize, move)
node BooleanDeclaration extends VariableDeclaration {
	style borderlessRectangleContainingText("Boolean", "${label} := ${parsedStartValue}")
	@possibleValuesProvider("info.scce.cinco.product.statechart.providers.BooleanValues")
	attr EString as startValue := "false"
	@propertiesViewHidden
	attr EBoolean as parsedStartValue := false
}

@palette("Declarations")
@disable(resize, move)
node IntegerDeclaration extends VariableDeclaration {
	style borderlessRectangleContainingText("Integer", "${label} := ${parsedStartValue}")
	attr EInt as startValue := "0"
	@propertiesViewHidden
	attr EInt as parsedStartValue := "0"
}

@palette("Declarations")
@disable(resize, move)
node TriggerDeclaration extends Declaration {
	style borderlessRectangleContainingText("Trigger", "${label}")
}



/*** Transitions ***/

abstract edge Transition {
	// Intentionally left blank
}

// Transition edge from a Start, ForkAndJoin or History node to a state or another control flow element.
edge SimpleTransition extends Transition {
	style solidEdgeWithArrowAndText("")
}

// Transition edge from one BranchAndMerge node to a state or another control flow element.
// Text annotation: [guard](priority)
// Any attributes, that have default values, will not be displayed.	
edge GuardedTransition extends Transition {
	style solidEdgeWithArrowAndText("${empty guard || guard == '<No guard>' ? '' : '['.concat(guard).concat(']').concat(empty priority || priority == 0 || guard == 'else' ? '' : '('.concat(priority).concat(')'))}")
	@grammar("info.scce.cinco.product.statechart.dsl.guard.GuardExpression", "info.scce.cinco.product.statechart.dsl.guard.ui.internal.GuardActivator")
	attr EString as guard := '<No guard>'
	attr EInt as priority := "0"
}

// Transition edge from one state to another state or control flow element.
// Text annotation: trigger [guard](priority) / action
// Any attributes, that have default values, will not be displayed.
edge StateTransition extends Transition {
	style solidEdgeWithArrowAndText("${(trigger == '<Default trigger>' ? '' : trigger).concat(empty guard || guard == '<No guard>' ? '' : ' ['.concat(guard).concat(']').concat(empty priority || priority == 0 || guard == 'else' ? '' : '('.concat(priority).concat(')'))).concat(empty action || action == '<No action>' ? '' : ' / '.concat(action))}")
	@possibleValuesProvider("info.scce.cinco.product.statechart.providers.TriggerValues")
	attr EString as trigger := '<Default trigger>'
	@grammar("info.scce.cinco.product.statechart.dsl.guard.GuardExpression", "info.scce.cinco.product.statechart.dsl.guard.ui.internal.GuardActivator")
	attr EString as guard := '<No guard>'
	@grammar("info.scce.cinco.product.statechart.dsl.action.ActionExpression", "info.scce.cinco.product.statechart.dsl.action.ui.internal.ActionActivator")
	attr EString as action := '<No action>'
	attr EInt as priority := "0"
}



/*** StateChartNode ***/

@event("info.scce.cinco.product.statechart.events.StateChartNodeEvent")
abstract node StateChartNode {
	// Intentionally left blank
}



/*** Control Flow ***/

abstract node ControlFlowNode extends StateChartNode {
	incomingEdges(Association)
}

// Start node as an entry point of a state chart graph or a state.
// Must have exactly one outgoing transition edge.
@icon("icons/StartIcon.png")
@palette("Control flow")
@disable(resize)
node Start extends ControlFlowNode {
	style filledCircle
	incomingEdges(Association)
	outgoingEdges(SimpleTransition[1, 1])
}

// End node as an exit point of a state chart graph or a state.
// Must have at least one incoming transition edge.
@icon("icons/EndIcon.png")
@palette("Control flow")
@disable(resize)
node End extends ControlFlowNode {
	style circleContainingFilledCircle
	incomingEdges(Transition[1, *], Association)
}

// A diamond-shaped node for branching and merging paths (OR).
// Must have at least one incoming and one outgoing transition edge.
@icon("icons/BranchAndMergeIcon.png")
@palette("Control flow")
@disable(resize)
node BranchAndMerge extends ControlFlowNode {
	style diamond
	incomingEdges(Transition[1, *], Association)
	outgoingEdges(GuardedTransition[1, *])
}

abstract node History extends ControlFlowNode {
	incomingEdges(Transition[1, *], Association)
	outgoingEdges(SimpleTransition[1, *])
}

// History node for returning to a previous state on the same level
// after exiting via an abort transition.
@icon("icons/HistoryIcon.png")
@palette("Control flow")
@disable(resize)
node NormalHistory extends History {
	style circleContainingLetter("H")
}

// Atomic history node for returning to a previous state on the lowest level
// after exiting via an abort transition.
@icon("icons/AtomicHistoryIcon.png")
@palette("Control flow")
@disable(resize)
node AtomicHistory extends History {
	style circleContainingLetter("H*")
}



/*** States ***/

// A state contains at least one parallel lane.
// A state must have at least one incoming and one outgoing transition edge.
@icon("icons/StateIcon.png")
@palette("States")
@event("info.scce.cinco.product.statechart.events.StateEvent")
container State extends StateChartNode {
	style roundedRectangleWithTitle("${(empty label ? 'State' : label)}")
	@mcam_label
	attr EString as label := ""
	incomingEdges(Transition[1, *], Association)
	outgoingEdges(StateTransition[1, *])
	containableElements(ParallelLane[1, *], ParallelLaneDivider)
}

// A parallel lane may contain states, control flow nodes and comments.
@icon("icons/ParallelLaneIcon.png")
@palette("States")
@event("info.scce.cinco.product.statechart.events.ParallelLaneEvent")
@disable(resize, move, select)
container ParallelLane {
	style borderlessRoundedRectangle
	containableElements(Start[0, 1], End, BranchAndMerge, NormalHistory[0, 1], AtomicHistory[0, 1], State, Comment)
}

// A dashed line for dividing parallel lanes.
@palette("States")
@disable(resize, move, select, create, delete)
node ParallelLaneDivider {
	style dashedLine
}



/*** Miscellaneous ***/

// Association edge from a comment node to any other node.
edge Association {
	style dashedEdge
}

// Comment node for displaying additional information.
// May have several association edges to other nodes.
@icon("icons/CommentIcon.png")
@palette("Miscellaneous")
node Comment extends StateChartNode {
	style documentContainingText("${(empty comment ? '' : comment)}")
	@multiLine
	attr EString as comment := "Comment"
	incomingEdges(Association)
	outgoingEdges(Association)
}

// A swim lane may contain states and / or any control flow nodes.
@icon("icons/SwimLaneIcon.png")
@palette("Miscellaneous")
container SwimLane {
	style dashedRectangleContainingText("${(empty label ? '' : label)}")
	@mcam_label
	attr EString as label := ""
	containableElements(Start[0, 1], End, BranchAndMerge, State, Comment)
}
