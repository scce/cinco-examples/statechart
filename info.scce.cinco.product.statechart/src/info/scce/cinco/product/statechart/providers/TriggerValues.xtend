package info.scce.cinco.product.statechart.providers

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import info.scce.cinco.product.statechart.statechart.StateChart
import info.scce.cinco.product.statechart.statechart.Transition

import static extension info.scce.cinco.product.statechart.extensions.StateChartExtension.*
import static extension info.scce.cinco.product.statechart.extensions.TransitionExtension.*

class TriggerValues extends CincoValuesProvider<Transition, String> {

	override getPossibleValues(Transition it) {

		val map = newLinkedHashMap
		for (trigger: staticTriggers) {
			map.put(trigger, trigger)
		}
		for (trigger: (rootElement as StateChart).triggerDeclarations) {
			map.put(trigger.label, trigger.label)
		}
		return map

	}
	
}
