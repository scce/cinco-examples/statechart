package info.scce.cinco.product.statechart.providers

import de.jabc.cinco.meta.runtime.provider.CincoValuesProvider
import info.scce.cinco.product.statechart.statechart.BooleanDeclaration

class BooleanValues extends CincoValuesProvider<BooleanDeclaration, String> {

	override getPossibleValues(BooleanDeclaration it) {
		return #{'false' -> 'false', 'true' -> 'true'}
	}
	
}
