package info.scce.cinco.product.statechart.checks

import info.scce.cinco.product.statechart.mcam.modules.checks.StateChartCheck
import info.scce.cinco.product.statechart.statechart.State
import info.scce.cinco.product.statechart.statechart.StateChart

import static extension info.scce.cinco.product.statechart.extensions.StateExtension.*
import static extension info.scce.cinco.product.statechart.extensions.TransitionExtension.*

class StateCheck extends StateChartCheck {

	override check(StateChart stateChart) {
		for (it: stateChart.find(State)) {
			complexStateMustContainStart
			stateContainingEndMustHaveDefaultOutgoingTransition
			stateWithDefaultOutgoingTransitionMustHaveEndsInEachLane
			groupsOfTransitionsWithTheSameTrigger
		}
	}

	def complexStateMustContainStart(State it) {
		if (!simple) {
			for (lane: parallelLanes.filter[starts.nullOrEmpty]) { // Lanes without a start node
				addError(lane, 'Each parallel lane of a complex state must contain a start node.')
			}
		}
	}

	def stateContainingEndMustHaveDefaultOutgoingTransition(State it) {
		if (!ends.nullOrEmpty) { // State contains at least one end node
			if (outgoingStateTransitions.forall[trigger != defaultTrigger]) { // State has no default trigger
				addError(it, '''A state containing end nodes must have an outgoing transition with a "«defaultTrigger»".''')
			}
		}
	}
	
	def stateWithDefaultOutgoingTransitionMustHaveEndsInEachLane(State it) {
		if (outgoingStateTransitions.exists[trigger == defaultTrigger] && parallelLanes.exists[ends.nullOrEmpty]) {
			addError(it, 'A state with an default outgoing transition must have end nodes for each parallel lane.')
		}
	}
	
	def groupsOfTransitionsWithTheSameTrigger(State it) {
		val triggerGroups = newHashMap
		for (transition : outgoingStateTransitions) {
			var group = triggerGroups.get(transition.trigger)
			if (group === null) {
				group = newArrayList
				triggerGroups.put(transition.trigger, group)
			}
			group.add(transition)
		}
		for (triggerGroup : triggerGroups.values) {
			if (triggerGroup.size == 1 && !triggerGroup.head.guard.isDefaultGuard) {
				addError(it, '''The transition with the unique trigger "«triggerGroup.head.trigger»" must use the "«defaultGuard»" guard.''')
			}
			else if (triggerGroup.size > 1 && triggerGroup.forall[!guard.isElseGuard]) {
				addError(it, '''The set of transitions with the trigger "«triggerGroup.head.trigger»" must have exactly one transition with the "«elseGuard»" guard.''')
			}
			else if (triggerGroup.size > 1 && triggerGroup.exists[guard.isDefaultGuard]) {
				addError(it, '''The set of transitions with the trigger "«triggerGroup.head.trigger»" must not use the "«defaultGuard»" guard.''')
			}
			if (triggerGroup.map[guard].containsDuplicates) {
				addError(it, '''The set of transitions with the trigger "«triggerGroup.head.trigger»" must have distinct guards.''')
			}
			if (triggerGroup.filter[!isDefaultGuard && !isElseGuard].map[priority].containsDuplicates) {
				addError(it, '''The set of transitions with the trigger "«triggerGroup.head.trigger»" must have distinct priorities.''')
			}
		}
	}

}
