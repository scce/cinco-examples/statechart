package info.scce.cinco.product.statechart.checks

import info.scce.cinco.product.statechart.mcam.modules.checks.StateChartCheck
import info.scce.cinco.product.statechart.statechart.BranchAndMerge
import info.scce.cinco.product.statechart.statechart.StateChart

import static extension info.scce.cinco.product.statechart.extensions.TransitionExtension.*

class BranchAndMergeCheck extends StateChartCheck {

	override check(StateChart stateChart) {
		for (it: stateChart.find(BranchAndMerge)) {
			
			if (outgoingGuardedTransitions.size == 1 && !outgoingGuardedTransitions.head.guard.isDefaultGuard) {
				addError(it, '''Nodes with only one branch must use the "«defaultGuard»" guard.''')
			}
			else if (outgoingGuardedTransitions.size > 1 && outgoingGuardedTransitions.forall[!guard.isElseGuard]) {
				addError(it, '''Nodes with multiple branches must have exactly one branch with the "«elseGuard»" guard.''')
			}
			else if (outgoingGuardedTransitions.size > 1 && outgoingGuardedTransitions.exists[guard.isDefaultGuard]) {
				addError(it, '''Nodes with multiple branches must not use the "«defaultGuard»" guard.''')
			}
			if (outgoingGuardedTransitions.map[guard].containsDuplicates) {
				addError(it, '''Nodes with multiple branches must have distinct guards.''')
			}
			if (outgoingGuardedTransitions.filter[!isDefaultGuard && !isElseGuard].map[priority].containsDuplicates) {
				addError(it, '''Nodes with multiple branches must have distinct priorities.''')
			}
			
		}
	}

}
