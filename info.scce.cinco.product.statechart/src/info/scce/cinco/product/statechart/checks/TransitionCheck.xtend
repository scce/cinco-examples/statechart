package info.scce.cinco.product.statechart.checks

import info.scce.cinco.product.statechart.mcam.modules.checks.StateChartCheck
import info.scce.cinco.product.statechart.statechart.StateChart
import info.scce.cinco.product.statechart.statechart.Transition

import static extension info.scce.cinco.product.statechart.extensions.NodeExtension.*
import static extension info.scce.cinco.product.statechart.extensions.TransitionExtension.*

class TransitionCheck extends StateChartCheck {

	override check(StateChart stateChart) {
		for (it: stateChart.find(Transition)) {
			
			if (!isDefaultGuard && !isElseGuard && priority < 0) {
				addWarning(it, '''Transitions should have positive priority values.''')
			}
			if (sourceElement.parentContainer !== targetElement.parentContainer) {
				addError(it, '''Transitions must not span across element hierarchies.''')
			}
			
		}
	}

}
