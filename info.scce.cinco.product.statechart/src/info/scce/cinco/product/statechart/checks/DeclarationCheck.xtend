package info.scce.cinco.product.statechart.checks

import info.scce.cinco.product.statechart.mcam.modules.checks.StateChartCheck
import info.scce.cinco.product.statechart.statechart.Declaration
import info.scce.cinco.product.statechart.statechart.StateChart
import info.scce.cinco.product.statechart.statechart.TriggerDeclaration
import info.scce.cinco.product.statechart.statechart.VariableDeclaration

import static info.scce.cinco.product.statechart.extensions.TransitionExtension.*

import static extension info.scce.cinco.product.statechart.extensions.DeclarationContainerExtension.*
import static extension info.scce.cinco.product.statechart.extensions.DeclarationExtension.*

class DeclarationCheck extends StateChartCheck {

	override check(StateChart stateChart) {
		for (it: stateChart.find(Declaration)) {
			labelMustBeId
			labelMustBeUnique
			reservedDeclarationLabels
		}
	}
	
	def labelMustBeId(Declaration it) {
		if (label.nullOrEmpty) {
			addError(it, '''The declaration label must not be empty. It must start with a letter followed by letters, numbers and / or "_".''')
		}
		else if (!label.matches('[a-zA-Z][a-zA-Z0-9_]*')) {
			addError(it, '''"«label»" is not a valid declaration label. It must start with a letter followed by letters, numbers and / or "_".''')
		}
	}

	def dispatch labelMustBeUnique(VariableDeclaration it) {
		if (declarationContainer.variableDeclarations.exists[d | d !== it && d.label == label]) {
			addError(it, '''All variables must be distinct.''')
		}
	}
	
	def dispatch labelMustBeUnique(TriggerDeclaration it) {
		if (declarationContainer.triggerDeclarations.exists[d | d !== it && d.label == label]) {
			addError(it, '''All triggers must be distinct.''')
		}
	}
	
	def dispatch reservedDeclarationLabels(VariableDeclaration it) {
		if (staticGuards.contains(label) || staticActions.contains(label)) {
			addError(it, '''The variable "«label»" is reserved. Please rename it.''')
		}
	}
	
	def dispatch reservedDeclarationLabels(TriggerDeclaration it) {
		if (staticTriggers.contains(label)) {
			addError(it, '''The trigger "«label»" is reserved. Please rename it.''')
		}
	}

}
