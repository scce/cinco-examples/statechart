// Generated by de.jabc.cinco.meta.plugin.event.generator.template.EventUserClassTemplate

package info.scce.cinco.product.statechart.events

import graphmodel.Direction
import info.scce.cinco.product.statechart.statechart.State
import info.scce.cinco.product.statechart.statechart.event.InternalStateEvent

import static extension info.scce.cinco.product.statechart.extensions.StateExtension.*

/* 
 * About this class:
 * - This is a default implementation for InternalStateEvent.
 * - This class was generated, because you added an "@event" annotation to
 *   Container "State" in "StateChart.mgl".
 * - This file will not be overwritten on future generation processes.
 * 
 * Edit this class:
 * - If you wish State to react the same way as its super class StateChartNode,
 *   you may delete the method or leave it as is (with only the super call).
 * - If you wish to only add functionality, leave the super call in the
 *   corresponding method and add your code to it.
 * - If you wish to break the inheritance chain, remove the super call, but do
 *   not delete the corresponding method. You may leave it empty or write new
 *   code.
 * 
 * Available event methods:
 * - canAttributeChange(State element, String attribute, Object newValue)
 * - preAttributeChange(State element, String attribute, Object newValue)
 * - postAttributeChange(State element, String attribute, Object oldValue)
 * - canCreate(Class<? extends State> elementClass, ModelElementContainer container, int x, int y, int width, int height)
 * - preCreate(Class<? extends State> elementClass, ModelElementContainer container, int x, int y, int width, int height)
 * - postCreate(State element)
 * - canDelete(State element)
 * - preDelete(State element)
 * - postDelete(State element)
 * - canDoubleClick(State element)
 * - postDoubleClick(State element)
 * - canMove(State element, ModelElementContainer newContainer, int newX, int newY)
 * - preMove(State element, ModelElementContainer newContainer, int newX, int newY)
 * - postMove(State element, ModelElementContainer oldContainer, int oldX, int oldY)
 * - canResize(State element, int newWidth, int newHeight, int newX, int newY, Direction direction)
 * - preResize(State element, int newWidth, int newHeight, int newX, int newY, Direction direction)
 * - postResize(State element, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction)
 * - canSelect(State element)
 * - postSelect(State element)
 */
final class StateEvent extends InternalStateEvent {
	
	override postCreate(State it) {
		
		// Create initial parallel lane for this new state
		newParallelLane(0, 0)
		resizeToDefualtSize
		
		super.postCreate(it)
		
	}
	
	override postDoubleClick(State it) {
		
		resizeToFitContents
		
		super.postDoubleClick(it)
		
	}
		
	override postResize(State it, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction) {

		enlargeToFitContents
		
		super.postResize(it, oldWidth, oldHeight, oldX, oldY, direction)
		
	}
	
}
