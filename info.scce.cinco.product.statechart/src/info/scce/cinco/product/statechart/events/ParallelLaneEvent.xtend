// Generated by de.jabc.cinco.meta.plugin.event.generator.template.EventUserClassTemplate

package info.scce.cinco.product.statechart.events

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import info.scce.cinco.product.statechart.statechart.ParallelLane
import info.scce.cinco.product.statechart.statechart.State
import info.scce.cinco.product.statechart.statechart.event.InternalParallelLaneEvent

import static extension info.scce.cinco.product.statechart.extensions.NodeExtension.*
import static extension info.scce.cinco.product.statechart.extensions.StateExtension.*

/* 
 * About this class:
 * - This is a default implementation for InternalParallelLaneEvent.
 * - This class was generated, because you added an "@event" annotation to
 *   Container "ParallelLane" in "StateChart.mgl".
 * - This file will not be overwritten on future generation processes.
 * 
 * Available event methods:
 * - canAttributeChange(ParallelLane element, String attribute, Object newValue)
 * - preAttributeChange(ParallelLane element, String attribute, Object newValue)
 * - postAttributeChange(ParallelLane element, String attribute, Object oldValue)
 * - canCreate(Class<? extends ParallelLane> elementClass, ModelElementContainer container, int x, int y, int width, int height)
 * - preCreate(Class<? extends ParallelLane> elementClass, ModelElementContainer container, int x, int y, int width, int height)
 * - postCreate(ParallelLane element)
 * - canDelete(ParallelLane element)
 * - preDelete(ParallelLane element)
 * - postDelete(ParallelLane element)
 * - canDoubleClick(ParallelLane element)
 * - postDoubleClick(ParallelLane element)
 * - canMove(ParallelLane element, ModelElementContainer newContainer, int newX, int newY)
 * - preMove(ParallelLane element, ModelElementContainer newContainer, int newX, int newY)
 * - postMove(ParallelLane element, ModelElementContainer oldContainer, int oldX, int oldY)
 * - canResize(ParallelLane element, int newWidth, int newHeight, int newX, int newY, Direction direction)
 * - preResize(ParallelLane element, int newWidth, int newHeight, int newX, int newY, Direction direction)
 * - postResize(ParallelLane element, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction)
 * - canSelect(ParallelLane element)
 * - postSelect(ParallelLane element)
 */
final class ParallelLaneEvent extends InternalParallelLaneEvent {
	
	extension GraphModelExtension = new GraphModelExtension
	
	override postCreate(ParallelLane it) {
		
		// Enlarge parent state
		parentState.alignLanes(true)
		parentState.enlargeToFitContents
		
	}
	
	override postDelete(ParallelLane it) {
		
		// Pre delete setup
		val parentState = findParents(State).head
		val oldLaneHeight = parentState.laneHeight
		
		// Post delete runnable
		return [
							
			// If the last lane was deleted, add a new empty lane
			if (parentState.parallelLanes.size == 0) {
				parentState.newParallelLane(0, 0)
			}
			
			// Reset to previous lane height
			parentState.laneHeight = oldLaneHeight
						
		]
		
	}
	
}
