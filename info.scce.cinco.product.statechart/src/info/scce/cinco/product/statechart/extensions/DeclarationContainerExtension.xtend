package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.Declaration
import info.scce.cinco.product.statechart.statechart.DeclarationContainer
import info.scce.cinco.product.statechart.statechart.SortOrder
import java.util.Collections

class DeclarationContainerExtension {
	
	public static val minWidth = 150
	public static val headerHeight = 20
	public static val declarationHeight = 20
	public static val gutter = 5
	
	/**
	 * Returns all declarations of this declaration container.
	 */
	static def getDeclarations(DeclarationContainer it) {
		val declarations = newLinkedList
		declarations.addAll(variableDeclarations)
		declarations.addAll(triggerDeclarations)
		return declarations
	}
	
	/**
	 * Returns all variable declarations of this declaration container.
	 */
	static def getVariableDeclarations(DeclarationContainer it) {
		val variables = newLinkedList
		variables.addAll(booleanDeclarations)
		variables.addAll(integerDeclarations)
		return variables
	}
	
	/**
	 * Returns all declarations of this declaration container in the specified
	 * order.
	 * @param order The order to sort the declarations.
	 */
	static def getOrderedDeclarations(DeclarationContainer it, SortOrder order) {
		val list = newLinkedList
		switch (order) {
			case SortOrder.TYPE: {
				list.addAll(booleanDeclarations.sort)
				list.addAll(integerDeclarations.sort)
				list.addAll(triggerDeclarations.sort)
			}
			case SortOrder.NAME: {
				list.addAll(declarations.sort)
			}
		}
		return list
	}
	
	/**
	 * Returns all declarations of this declaration container ordered by the
	 * {@code sortOrder} attribute of the declaration container.
	 */
	static def getOrderedDeclarations(DeclarationContainer it) {
		return getOrderedDeclarations(sortOrder)
	}
	
	/**
	 * Sorts and aligns all declaration of this declaration container. Resizes
	 * the declaration container to fit its containing declarations.
	 */
	static def align(DeclarationContainer it) {
		val sortedDeclarations = orderedDeclarations
		width = Math.max(minWidth, width)
		height = headerHeight + 2 * gutter + sortedDeclarations.size * declarationHeight
		for (i: 0 ..< sortedDeclarations.size) {
			val declaration = sortedDeclarations.get(i)
			declaration.width = width - 2 * gutter
			declaration.height = declarationHeight
			declaration.x = gutter
			declaration.y = headerHeight + gutter + i * declarationHeight
		}
	}
	
	/**
	 * Switches between sorting the declarations by name and by type. Afterwards,
	 * the declaration container is {@link #align(DeclarationContainer) aligned}.
	 */
	static def toggleSortOrder(DeclarationContainer it) {
		sortOrder = if (sortOrder === SortOrder.NAME) SortOrder.TYPE else SortOrder.NAME
		align
	}
	
	/**
	 * Private method for sorting a list / iterable of Declarations by their names.
	 * Returns a <strong>new</strong> list that is sorted.
	 */
	private static def sort(Iterable<? extends Declaration> it) {
		val list = newLinkedList
		list.addAll(it)
		Collections.sort(list)[a, b | a.label.compareTo(b.label)]
		return list
	}
	
}
