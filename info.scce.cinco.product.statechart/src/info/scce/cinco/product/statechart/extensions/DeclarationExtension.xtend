package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.Declaration
import info.scce.cinco.product.statechart.statechart.DeclarationContainer

class DeclarationExtension {
	
	/**
	 * Returns the declaration container of this declaration.
	 */
	static def getDeclarationContainer(Declaration it) {
		if (container instanceof DeclarationContainer) {
			return container as DeclarationContainer
		}
		else {
			return null
		}
	}
	
}
