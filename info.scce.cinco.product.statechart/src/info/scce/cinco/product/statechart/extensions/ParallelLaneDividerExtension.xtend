package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.ParallelLaneDivider

class ParallelLaneDividerExtension {
	
	/**
	 * Hides the divider by moving it out of the viewable area of the state node.
	 */
	static def hide(ParallelLaneDivider it) {
		width = 1
		x = -10
		y = -10
	}
	
}