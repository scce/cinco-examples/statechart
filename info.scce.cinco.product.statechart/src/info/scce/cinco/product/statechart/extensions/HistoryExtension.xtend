package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.History

class HistoryExtension {
	
	/**
	 * Returns the one outgoing simple transition of this history node.
	 * May be {@code null}.
	 */
	static def getOutgoingSimpleTransition(History it) {
		return outgoingSimpleTransitions.head
	}
	
	/**
	 * Returns the successor of this history node.
	 * May be {@code null}.
	 */
	static def getSuccessor(History it) {
		return successors.head
	}
	
}