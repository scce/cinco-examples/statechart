package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.State

import static extension info.scce.cinco.product.statechart.extensions.ParallelLaneDividerExtension.*
import static extension info.scce.cinco.product.statechart.extensions.ParallelLaneExtension.*

class StateExtension {
	
	public static val defaultWidth = 100
	public static val defaultHeight = 60
	public static val headerHeight = 20
	public static val gutter = 5
		
	/**
	 * Aligns all parallel lanes of a state, such that all lanes are distributed
	 * evenly inside the state's body. Surplus lane dividers will not be deleted.
	 * @param preserveLaneHeight
	 *            If {@code true}, the lane height will be preserved and the
	 *            state's height will be modified.
	 *            If {@code false}, the state's height will be preserved and
	 *            the lane height will be modified.
	 * @see #alignLanes(State)
	 */
	static def alignLanes(State it, boolean preserveLaneHeight) {
		
		// If there are no parallel lanes, hide all dividers
		if (parallelLanes.empty) {
			for (divider: parallelLaneDividers) {
				divider.hide
			}
			return
		}
		
		// Get lanes and dividers
		val lanes = parallelLanes.sortBy[y]
		val dividers = parallelLaneDividers.sortBy[y]
		
		// Adjust number of dividers
		val difference = (lanes.size - 1) - dividers.size
		if (difference > 0) {
			for (i: 1 .. difference) {
				dividers.add(newParallelLaneDivider(0, 0, width, 1))
			}
		}
		else if (difference < 0) {
			for (i: 1 .. -difference) {
				dividers.last.hide
				dividers.remove(dividers.size - 1)
			}
		}
		
		if (preserveLaneHeight) {
			height = headerHeight + (parallelLanes.size + 1) * laneHeight
		}
		
		// Align lanes
		for (i: 0 ..< lanes.size) {
			val lane = lanes.get(i)
			lane.x = gutter
			lane.y = headerHeight + i * laneHeight + gutter
			lane.width = width - 2 * gutter
			lane.height = laneHeight - 2 * gutter
		}
		
		// Align dividers
		for (i: 0 ..< dividers.size) {
			val divider = dividers.get(i)
			divider.x = 0
			divider.y = headerHeight + laneHeight + i * laneHeight
			divider.width = width
			divider.height = 1
		}
		
	}
	
	/**
	 * Aligns all parallel lanes of a state, such that all lanes are distributed
	 * evenly inside the state's body. The state's height will be preserved and
	 * the lane height will be modified. Surplus lane dividers will not be deleted.
	 * @see #alignLanes(State, boolean)
	 */
	static def alignLanes(State it) {
		alignLanes(false)
	}
	
	/**
	 * Shrinks the state to its default small size, if it does not contain any nodes.
	 * Enlarges state to its default large size or larger, if it does contain any nodes.
	 */
	static def resizeToDefualtSize(State it) {
		
		if (simple) {
			// Set "simple" states to default small size
			width = defaultWidth
			height = defaultHeight
		}
		else {
			// Set "complex" states to default large size
			width = ParallelLaneExtension.defaultWidth
			laneHeight = ParallelLaneExtension.defaultHeight
		}
		
		alignLanes
		
	}
	
	/**
	 * Shrinks the state to its default small size, if it does not contain any nodes.
	 * Resizes state to fit its contents exactly, if it does contain any nodes.
	 */
	static def resizeToFitContents(State it) {
		
		if (simple) {
			// Set "simple" states to default small size
			width = defaultWidth
			height = defaultHeight
		}
		else {
			// Set "complex" states to fit their contents or larger
			width = maxContentWidth
			laneHeight = maxContentHeight
		}
		
		alignLanes
		
	}
	
	/**
	 * Shrinks the state to its default small size or larger, if it does not contain any nodes.
	 * Enlarges state to fit its contents or larger, if it does contain any nodes.
	 */
	static def enlargeToFitContents(State it) {
		
		if (simple) {
			// Set "simple" states to default small size
			width = Math.max(defaultWidth, width)
			height = Math.max(defaultHeight, height)
		}
		else {
			// Set "complex" states to fit their contents or larger
			width = Math.max(maxContentWidth, width)
			height = Math.max(headerHeight + parallelLanes.size * maxContentHeight, height)
		}
		
		alignLanes
		
	}
	
	/**
	 * Shrinks the state to its default small size, if it does not contain any nodes.
	 * Shrinks state to fit its contents or smaller, if it does contain any nodes.
	 */
	static def shrinkToFitContents(State it) {
		
		if (simple) {
			// Set "simple" states to default small size
			width = defaultWidth
			height = defaultHeight
		}
		else {
			// Set "complex" states to fit their contents or smaller
			width = Math.min(maxContentWidth, width)
			height = Math.min(headerHeight + parallelLanes.size * maxContentHeight, height)
		}
		
		alignLanes
		
	}
	
	/**
	 * A state is simple, if it has exactly one parallel lane and this lane does not contain any nodes.
	 * @see #isComplex(State) isComplex(State)
	 */
	static def isSimple(State it) {
		return parallelLanes.size == 1 && parallelLanes.head.nodes.empty
	}
	
	/**
	 * A state is complex, if it has more than one parallel lane or its lanes contain any nodes.
	 * @see #isSimple(State) isSimple(State)
	 */
	static def isComplex(State it) {
		return !simple
	}
	
	/**
	 * The maximum width of the contents of all lanes of this state.
	 */
	static def getMaxContentWidth(State it) {
		return parallelLanes.map[contentWidth].max + 2 * gutter
	}
	
	/**
	 * The maximum Height of the contents of all lanes of this state.
	 */
	static def getMaxContentHeight(State it) {
		return parallelLanes.map[contentHeight].max + 2 * gutter
	}
	
	/**
	 * Calculates the lane height from the current height of the state.
	 */
	static def getLaneHeight(State it) {
		return (height - headerHeight) / parallelLanes.size
	}
	
	/**
	 * Sets the height of this state, so that each lane has the <code>newLaneHeight</code>.
	 */
	static def setLaneHeight(State it, int newLaneHeight) {
		height = headerHeight + parallelLanes.size * newLaneHeight
		alignLanes
		return newLaneHeight
	}
	
	/**
	 * Deletes surplus dividers.
	 */
	static def pruneDividers(State it) {
		val difference = parallelLaneDividers.size - (parallelLanes.size - 1)
		if (difference > 0) {
			for (int i: 1 .. difference) {
				parallelLaneDividers.last.delete
			}
		}
		alignLanes
	}
	
	/**
	 * Returns a list of all start nodes of this state.
	 */
	static def getStarts(State it) {
		return parallelLanes.map[start]
	}
	
	/**
	 * Returns a list of all end nodes of this state.
	 */
	static def getEnds(State it) {
		return parallelLanes.map[ends].flatten.toList
	}
	
	/**
	 * Returns a list of all state nodes of this state.
	 */
	static def getStates(State it) {
		return parallelLanes.map[states].flatten.toList
	}
	
}