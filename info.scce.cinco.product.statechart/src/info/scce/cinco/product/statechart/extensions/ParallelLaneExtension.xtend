package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.ParallelLane

class ParallelLaneExtension {
	
	public static val defaultWidth = 450
	public static val defaultHeight = 100
	
	/**
	 * The minimum width for the parallel lane in order to display all
	 * containing nodes. The return value is at least the default width.
	 */
	static def getContentWidth(ParallelLane it) {
		if (nodes.empty) {
			return defaultWidth
		}
		else {
			return Math.max(defaultWidth, nodes.map[x + width].max)
		}
	}
	
	/**
	 * The minimum width for the parallel lane in order to display all
	 * containing nodes. The return value is at least the default width.
	 */
	static def getContentHeight(ParallelLane it) {
		if (nodes.empty) {
			return defaultHeight
		}
		else {
			return Math.max(defaultHeight, nodes.map[y + height].max)
		}
		
	}
	
	/**
	 * Returns {@code true}, if the parallel lane contains a normal history
	 * and/or an atomic history node.
	 */
	static def hasHistory(ParallelLane it) {
		return hasNormalHistory || hasAtomicHistory
	}
	
	/**
	 * Returns {@code true}, if the parallel lane contains a normal history
	 * node.
	 */
	static def hasNormalHistory(ParallelLane it) {
		return !normalHistorys.nullOrEmpty
	}
	
	/**
	 * Returns {@code true}, if the parallel lane contains an atomic history
	 * node.
	 */
	static def hasAtomicHistory(ParallelLane it) {
		return !atomicHistorys.nullOrEmpty
	}
	
	/**
	 * Returns {@code null}, if the parallel lane does not contain a normal
	 * history node.
	 */
	static def getNormalHistory(ParallelLane it) {
		return normalHistorys.head
	}
	
	/**
	 * Returns {@code null}, if the parallel lane does not contain an atomic
	 * history node.
	 */
	static def getAtomicHistory(ParallelLane it) {
		return atomicHistorys.head
	}
	
	/**
	 * Returns the start node of this parallel lane.
	 */
	static def getStart(ParallelLane it) {
		return starts.head
	}
	
}