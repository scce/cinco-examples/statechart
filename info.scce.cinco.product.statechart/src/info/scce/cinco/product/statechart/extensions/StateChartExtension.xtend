package info.scce.cinco.product.statechart.extensions

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import info.scce.cinco.product.statechart.statechart.StateChart

import static extension info.scce.cinco.product.statechart.extensions.DeclarationContainerExtension.*

class StateChartExtension {

	static extension WorkbenchExtension = new WorkbenchExtension
	static extension WorkspaceExtension = new WorkspaceExtension
	
	static StateChart stateChart
	
	/**
	 * Retrieves the state chart that currently is edited in the active editor
	 * in the active workbench window. Returns {@code null} if any of these
	 * entities cannot be retrieved instead of throwing exceptions.
	 */
	static synchronized def StateChart getActiveStateChart() {
		var StateChart localStateChart
		stateChart = null
		
		sync[
			val agm = activeGraphModel
			if (agm instanceof StateChart) {
				stateChart = agm as StateChart
			}
		]
		
		localStateChart = stateChart
		stateChart = null
		
		return localStateChart
	}
	
	/**
	 * Opens the state chart in an editor.
	 */
	static def show(StateChart it) {
		return openEditor
	}
	
	/**
	 * Returns the (file) name of the state chart.
	 */
	static def getName(StateChart it) {
		val file = file
		val fileExtension = '.' + file.fileExtension
		return file.name.replace(fileExtension, '')
	}
	
	/**
	 * Returns the start node of this state chart.
	 */
	static def getStart(StateChart it) {
		return starts.head
	}
	
	/**
	 * Returns the declaration container of this state chart.
	 */
	static def getDeclarationContainer(StateChart it) {
		return declarationContainers.head
	}
	
	/**
	 * Returns all declarations of this state chart.
	 */
	static def getDeclarations(StateChart it) {
		if (declarationContainer === null) return #[]
		return declarationContainer.declarations
	}
	
	/**
	 * Returns all variables of this state chart.
	 */
	static def getVariableDeclarations(StateChart it) {
		if (declarationContainer === null) return #[]
		return declarationContainer.variableDeclarations
	}
	
	/**
	 * Returns the boolean declarations of this state chart.
	 */
	static def getBooleanDeclarations(StateChart it) {
		if (declarationContainer === null) return #[]
		return declarationContainer.booleanDeclarations
	}
	
	/**
	 * Returns the integer declarations of this state chart.
	 */
	static def getIntegerDeclarations(StateChart it) {
		if (declarationContainer === null) return #[]
		return declarationContainer.integerDeclarations
	}
	
	/**
	 * Returns the trigger declarations of this state chart.
	 */
	static def getTriggerDeclarations(StateChart it) {
		if (declarationContainer === null) return #[]
		return declarationContainer.triggerDeclarations
	}
	
	/**
	 * Returns the labels of all variables of this state chart.
	 */
	static def getVariableLabels(StateChart it) {
		return triggerDeclarations.map[label]
	}
	
	/**
	 * Returns the labels of the boolean declarations of this state chart.
	 */
	static def getBooleanLabels(StateChart it) {
		return booleanDeclarations.map[label]
	}
	
	/**
	 * Returns the labels of the integer declarations of this state chart.
	 */
	static def getIntegerLabels(StateChart it) {
		return integerDeclarations.map[label]
	}
	
	/**
	 * Returns the labels of the trigger declarations of this state chart.
	 */
	static def getTriggerLabels(StateChart it) {
		return triggerDeclarations.map[label]
	}
	
}