package info.scce.cinco.product.statechart.extensions

import graphmodel.Container
import graphmodel.Node
import info.scce.cinco.product.statechart.statechart.State
import info.scce.cinco.product.statechart.statechart.StateChart
import java.util.LinkedList

class NodeExtension {
	
	/**
	 * Returns the state chart of the provided node. If the node has no state
	 * chart as a root element, {@code null} will be returned.
	 */
	static def StateChart getStateChart(Node it) {
		if (rootElement !== null && rootElement instanceof StateChart) {
			return rootElement as StateChart
		}
		else {
			return null
		}
	}
	
	/**
	 * Returns the parent container of the provided node. If the node has no
	 * parent, {@code null} will be returned.
	 */
	static def Container getParentContainer(Node it) {
		if (container instanceof Container) {
			return container as Container
		}
		else {
			return null
		}
	}
	
	/**
	 * Returns a list of all ancestor containers of the provided node. The
	 * list is ordered form the deepest containment level to the root level.
	 * The list does neither include the provided state object nor the root
	 * container.
	 */
	static def LinkedList<Container> getAncestorContainers(Node it) {
		if (parentContainer === null) {
			return newLinkedList
		}
		else {
			val ancestorsList = parentContainer.ancestorContainers
			ancestorsList.addFirst(parentContainer)
			return ancestorsList
		}
	}
	
	/**
	 * Returns the parent state of the provided node. If the node has no parent
	 * state, {@code null} will be returned.
	 */
	static def State getParentState(Node it) {
		var parent = parentContainer
		while (parent !== null && !(parent instanceof State)) {
			parent = parent.parentContainer
		}
		if (parent instanceof State) {
			return parent as State
		}
		else {
			return null
		}
	}
	
	/**
	 * Returns a list of all ancestor states of the provided node. The list is
	 * ordered form the deepest containment level to the root level. The list
	 * does neither include the provided state object nor the root container.
	 */
	static def LinkedList<State> getAncestorStates(State it) {
		if (parentState === null) {
			return newLinkedList
		}
		else {
			val ancestorsList = parentState.ancestorStates
			ancestorsList.addFirst(parentState)
			return ancestorsList
		}
	}
	
}