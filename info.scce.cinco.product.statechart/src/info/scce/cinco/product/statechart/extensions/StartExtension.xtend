package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.Start

class StartExtension {
	
	/**
	 * Returns the one outgoing simple transition of this start node.
	 * May be {@code null}.
	 */
	static def getOutgoingSimpleTransition(Start it) {
		return outgoingSimpleTransitions.head
	}
	
	/**
	 * Returns the successor of this start node.
	 * May be {@code null}.
	 */
	static def getSuccessor(Start it) {
		return successors.head
	}
	
}