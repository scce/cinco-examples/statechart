package info.scce.cinco.product.statechart.extensions

import info.scce.cinco.product.statechart.statechart.GuardedTransition
import info.scce.cinco.product.statechart.statechart.StateTransition
import info.scce.cinco.product.statechart.statechart.Transition

class TransitionExtension {
	
	public static val defaultTrigger = '<Default trigger>'
	public static val staticTriggers = #[defaultTrigger]
	
	public static val defaultGuard = '<No guard>'
	public static val elseGuard = 'else'
	public static val staticGuards = #[defaultGuard, elseGuard]
	
	public static val defaultAction = '<No action>'
	public static val staticActions = #[defaultAction]
	
	/**
	 * Returns {@code true}, if the transition has a non-empty, non-null trigger.
	 */
	static def hasTrigger(Transition it) {
		return !trigger.nullOrEmpty // && !staticTriggers.contains(trigger)
	}
	
	/**
	 * Returns {@code true}, if the transition has a non-empty, non-null guard.
	 */
	static def hasGuard(Transition it) {
		return !guard.nullOrEmpty // && !staticGuards.contains(guard)
	}
	
	/**
	 * Returns {@code true}, if the transition has a non-empty, non-null action.
	 */
	static def hasAction(Transition it) {
		return !action.nullOrEmpty // && !staticActions.contains(action)
	}
	
	/**
	 * Returns {@code true}, if the transition has a priority.
	 */
	static def hasPriority(Transition it) {
		return priority > Integer.MIN_VALUE
	}
	
	/**
	 * Returns the transition's trigger. Returns {@code null}, if this the type
	 * of transition has no trigger.
	 */
	static def getTrigger(Transition it) {
		return switch it {
			StateTransition: trigger
			default:         null
		}
	}
	
	/**
	 * Returns the transition's guard. Returns {@code null}, if this the type
	 * of transition has no guard.
	 */
	static def getGuard(Transition it) {
		return switch it {
			StateTransition:   guard
			GuardedTransition: guard
			default:           null
		}
	}
	
	/**
	 * Returns the transition's action. Returns {@code null}, if this the type
	 * of transition has no action.
	 */
	static def getAction(Transition it) {
		return switch it {
			StateTransition: action
			default:         null
		}
	}
	
	/**
	 * Returns the transition's priority. Returns {@link Integer#MIN_VALUE
	 * Integer.MIN_VALUE}, if this the type of transition has no priority.
	 */
	static def getPriority(Transition it) {
		return switch it {
			StateTransition:   priority
			GuardedTransition: priority
			default:           Integer.MIN_VALUE
		}
	}
	
	/**
	 * Returns the transition's "siblings" ( == all outgoing transitions of this
	 * transition's source node). The transitions itself is omitted from the result.
	 */
	static def getSiblings(Transition it) {
		sourceElement.outgoing.filter(Transition).filter[t | t !== it].toList
	}
	
	/**
	 * Returns {@code true} if the String represents a default guard.
	 */
	static def isDefaultGuard(String guard) {
		if (guard === null) return true
		val trimmed = guard.trim
		return trimmed.empty || trimmed == defaultGuard
	}
	
	/**
	 * Returns {@code true} if the String represents an "else" guard.
	 */
	static def isElseGuard(String guard) {
		if (guard === null) return false
		return guard.trim == elseGuard
	}
	
	/**
	 * Returns {@code true} if the Transition has a default guard.
	 */
	static def isDefaultGuard(Transition it) {
		guard.isDefaultGuard
	}
	
	/**
	 * Returns {@code true} if the Transition has an "else" guard.
	 */
	static def isElseGuard(Transition it) {
		guard.isElseGuard
	}
	
}