package info.scce.cinco.product.statechart.extensions

import graphmodel.Container
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.cinco.product.statechart.statechart.State
import java.util.LinkedList

class ContainerExtension {
	
	/**
	 * Returns a list of all direct child model elements of this container.
	 * This includes its child nodes, as well as the incoming edges of its
	 * child nodes.
	 */
	static def LinkedList<ModelElement> getChildElements(Container it) {
		val LinkedList<ModelElement> childrenList = newLinkedList
		childrenList.addAll(nodes)
		nodes.forEach[childrenList.addAll(incoming)]
		return childrenList
	}
	
	/**
	 * Returns a list of all descendant model elements of this container.
	 * This includes its child nodes, as well as the incoming edges of its
	 * child nodes, as well as as the children's children and so on ...
	 */
	static def LinkedList<ModelElement> getDescendantElements(Container it) {
		val LinkedList<ModelElement> descendantsList = newLinkedList
		descendantsList.addAll(childElements)
		childElements.filter(Container).forEach[childContainer |
			descendantsList.addAll(childContainer.descendantElements)
		]
		return descendantsList
	}
	
	/**
	 * Returns a list of all direct child nodes of this container.
	 */
	static def LinkedList<Node> getChildNodes(Container it) {
		val LinkedList<Node> childrenList = newLinkedList
		childrenList.addAll(nodes)
		return childrenList
	}
	
	/**
	 * Returns a list of all descendant nodes of this container.
	 */
	static def LinkedList<Node> getDescendantNodes(Container it) {
		val LinkedList<Node> descendantsList = newLinkedList
		descendantsList.addAll(childNodes)
		childNodes.filter(Container).forEach[childContainer |
			descendantsList.addAll(childContainer.descendantNodes)
		]
		return descendantsList
	}
	
	/**
	 * Returns a list of all descendant states of the provided container.
	 * The list does not include the provided state.
	 */
	static def LinkedList<State> getDescendantStates(Container it) {
		val LinkedList<State> descendantsList = newLinkedList
		descendantsList.addAll(descendantNodes.filter(State))
		return descendantsList
	}
	
}