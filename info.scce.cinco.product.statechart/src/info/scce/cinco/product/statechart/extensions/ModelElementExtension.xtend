package info.scce.cinco.product.statechart.extensions

import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlight
import graphmodel.Container
import graphmodel.Edge
import graphmodel.ModelElement
import graphmodel.Node
import java.util.HashMap

class ModelElementExtension {
	
	static HashMap<ModelElement, Highlight> highlightsInstance = null
	static HashMap<ModelElement, String> foregroundColorsInstance = null
	static HashMap<ModelElement, String> backgroundColorsInstance = null
//	static String defaultForegroundColor = '0000FF' // Blue
	static String defaultBackgroundColor = 'FF0000' // Red
	
	/**
	 * Returns a singleton instance of the highlights map. It maps from
	 * model elements to a highlight object.
	 */
	private static def HashMap<ModelElement, Highlight> getHighlights() {
		if(highlightsInstance === null) {
			highlightsInstance = newHashMap
		}
		return highlightsInstance
	}
	
	/**
	 * Returns a singleton instance of the foreground colors map. It maps from
	 * model elements to a highlight color.
	 */
	private static def HashMap<ModelElement, String> getForegroundColors() {
		if(foregroundColorsInstance === null) {
			foregroundColorsInstance = newHashMap
		}
		return foregroundColorsInstance
	}
	
	/**
	 * Returns a singleton instance of the background colors map. It maps from
	 * model elements to a highlight color.
	 */
	private static def HashMap<ModelElement, String> getBackgroundColors() {
		if(backgroundColorsInstance === null) {
			backgroundColorsInstance = newHashMap
		}
		return backgroundColorsInstance
	}
	
	/**
	 * Returns a hex representation of a model element's foreground color.
	 * Returns {@code null} if the state has no highlight color.
	 */
	static def getForegroundColor(ModelElement it) {
		return foregroundColors.get(it)
	}
	
	/**
	 * Returns a hex representation of a model element's background color.
	 * Returns {@code null} if the state has no highlight color.
	 */
	static def getBackgroundColor(ModelElement it) {
		return backgroundColors.get(it)
	}
	
	/**
	 * Returns whether a model element has a set foreground highlight color.
	 */
	static def hasForegroundColor(ModelElement it) {
		return foregroundColor !== null
	}
	
	/**
	 * Returns whether a model element has a set background highlight color.
	 */
	static def hasBackgroundColor(ModelElement it) {
		return backgroundColor !== null
	}
	
	/**
	 * Sets the foreground color of a model element.
	 * @param hexColor A hex representation of the desired highlight color. If
	 *                 {@code null} or empty then the highlight will be removed.
	 */
	static def setForegroundColor(ModelElement it, String hexColor) {
		if (hexColor.nullOrEmpty) {
			foregroundColors.remove(it)
			highlighted = highlighted && hasBackgroundColor
		}
		else {
			foregroundColors.put(it, hexColor)
			highlighted = highlighted
		}
		return foregroundColor
	}
	
	/**
	 * Sets the background color of a model element.
	 * @param hexColor A hex representation of the desired highlight color. If
	 *                 {@code null} or empty then the highlight will be removed.
	 */
	static def setBackgroundColor(ModelElement it, String hexColor) {
		if (hexColor.nullOrEmpty) {
			backgroundColors.remove(it)
			highlighted = highlighted && hasForegroundColor
		}
		else {
			backgroundColors.put(it, hexColor)
			highlighted = highlighted
		}
		return backgroundColor
	}

	/**
	 * Returns {@code true}, if the model element is highlighted.
	 */
	static def isHighlighted(ModelElement it) {
		return highlights.get(it) !== null
	}
	
	/**
	 * Activates or deactivates the highlighting for this model element.
	 */
	static def setHighlighted(ModelElement it, boolean value) {
					
		// Remove old highlight
		val oldHighlight = highlights.remove(it)
		if (oldHighlight !== null) {
			oldHighlight.off()
			oldHighlight.remove(it)
		}
		
		// If the state shall be highlighted, create a new highlight
		if (value) {
			val newHighlight = new Highlight
			newHighlight.add(it)
			if (hasForegroundColor) {
				newHighlight.foregroundColor = foregroundColor
			}
			if (hasBackgroundColor) {
				newHighlight.backgroundColor = backgroundColor
			}
			if (!hasForegroundColor && !hasBackgroundColor) {
				newHighlight.backgroundColor = defaultBackgroundColor
			}
			newHighlight.on()
			highlights.put(it, newHighlight)
		}
		
		return value

	}
	
	/**
	 * Removes the model element's highlight and its stored fore- and
	 * background colors.
	 */
	static def removeHighlight(ModelElement it) {
		val highlight = highlights.remove(it)
		if (highlight !== null) {
			highlight.off()
			highlight.remove(it)
		}
		foregroundColors.remove(it)
		backgroundColors.remove(it)
	}
	
	/**
	 * Removes all highlights and the stored fore- and background colors.
	 */
	static def removeAllHighlights() {
		highlights.values.forEach[off]
		highlightsInstance = null
		foregroundColorsInstance = null
		backgroundColorsInstance = null
	}
	
	/**
	 * Returns the parent node of the provided model element. If the element
	 * has no parent node, {@code null} will be returned.
	 */
	static def parentNode(ModelElement it) {
		switch (it) {
			Node case container instanceof Container:
				container as Container
			Edge case targetElement.container instanceof Container:
				targetElement.container as Container
			default:
				null
		}
	}
	
}